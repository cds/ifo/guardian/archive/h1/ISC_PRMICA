# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: ISC_library.py 15278 2017-03-16 22:14:16Z kiwamu.izumi@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/isc/h1/guardian/ISC_library.py $

import cdsutils
import lscparams
import alsconst
from gpstime import gpstime
tconvert = gpstime.tconvert
import numpy 
import time

import fast_ezca as fez

##################################################
## utilities
##################################################
def grab_data(seconds,channel):
    now = int(tconvert('now').gps())
    con = cdsutils.nds.get_connection()
    buf = con.fetch(now-seconds-1,now-1,[channel])
    return buf[0].data

def checkpoint(chkpt):
    log('checkpoint '+str(chkpt))
    return

def asc_convergence_checker(loopsList, Pit_thresholds, Yaw_thresholds):
    ConvergenceFlag = True
    for ii in range(len(loopsList)):
        if abs(ezca['ASC-{}_P_OUT16'.format(loopsList[ii])]) > Pit_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Pit not converged, thresh={1}'.format(loopsList[ii], Pit_thresholds[ii]))
        if abs(ezca['ASC-{}_Y_OUT16'.format(loopsList[ii])]) > Yaw_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Yaw not converged, thresh={1}'.format(loopsList[ii], Yaw_thresholds[ii]))
    return ConvergenceFlag

def smooth_offload(optic, ramptime):
    dofs = ['Y','P'] 
    log(optic)
    if optic[:3] == ('ETM'):
        top_stage = 'M0'
    elif optic[:3] == ('ITM'):
        top_stage = 'M0'
    else:
        top_stage = 'M1'
    log(top_stage)
    # set up LIGOFilters
    # store in dicts indexed by dof
    lock_fm = {}
    drivealign_fm = {}
    opticalign_fm = {}

    for dof in dofs:
        lock_fm[dof] = ezca.get_LIGOFilter('SUS-{0}_{1}_LOCK_{2}'.format(optic, top_stage, dof))
        drivealign_fm[dof] = ezca.get_LIGOFilter('SUS-{0}_{1}_DRIVEALIGN_{2}2{2}'.format(optic, top_stage, dof))
        opticalign_fm[dof] = ezca.get_LIGOFilter('SUS-{0}_{1}_OPTICALIGN_{2}'.format(optic, top_stage, dof))

    # record initial state
    # store in dicts indexed by dof
    drivealign_output = {}
    lock_gain = {}
    lock_tramp = {}
    opticalign_tramp = {}

    for dof in dofs:
        lock_gain[dof] = lock_fm[dof].GAIN.get()
        lock_tramp[dof] = lock_fm[dof].TRAMP.get()
        opticalign_tramp[dof] = opticalign_fm[dof].TRAMP.get()

    # simultaneously ramp LOCK gain to zero, and
    # ramp OPTICALIGN offset to the new value
    for dof in dofs:
        desired_bias = opticalign_fm[dof].OFFSET.get()
        desired_bias += round((drivealign_fm[dof].OUTPUT.get()/opticalign_fm[dof].GAIN.get()), 4)
        lock_fm[dof].turn_off('INPUT')
        lock_fm[dof].ramp_gain(0, ramptime, wait=False)
        opticalign_fm[dof].ramp_offset(desired_bias, ramptime, wait=False)

    # wait for all ramps to complete (should use a timer for this)
    for dof in dofs:
        while lock_fm[dof].is_gain_ramping() or opticalign_fm[dof].is_offset_ramping():
            log('waiting')
            time.sleep(0.1)
    log('done waiting')
    # clear LOCK history
    for dof in dofs:
        lock_fm[dof].RSET.put(2)

    # restore initial state
    for dof in dofs:
        lock_fm[dof].turn_on('INPUT')
        lock_fm[dof].ramp_gain(lock_gain[dof], lock_tramp[dof], wait=False)
        opticalign_fm[dof].TRAMP.put(opticalign_tramp[dof])

    
    
def smooth_offload_fast(opticList, ramptime):
    log(opticList)

    # set up names for each optic and DOF to offload
    numOffload = 2 * len(opticList)
    n_dof = []
    n_optic = []
    n_top_stage = []

    for index in range(len(opticList)):
        optic = opticList[index]       # this optic

        # the top stage for this optic
        if optic[:3] == ('ETM'):
            top_stage = 'M0'
        elif optic[:3] == ('ITM'):
            top_stage = 'M0'
        else:
            top_stage = 'M1'

        # the optic, dof, and top stage for each index
        n_dof += ['P', 'Y']
        n_optic += [optic, optic]
        n_top_stage += [top_stage, top_stage]

    # set up LIGOFilters
    lock_fm = []
    drivealign_fm = []
    opticalign_fm = []

    for index in range(numOffload):
        dof = n_dof[index]
        optic = n_optic[index]
        top_stage = n_top_stage[index]

        lock_fm.append(ezca.get_LIGOFilter('SUS-{0}_{1}_LOCK_{2}'.format(optic, top_stage, dof)))
        drivealign_fm.append(ezca.get_LIGOFilter('SUS-{0}_{1}_DRIVEALIGN_{2}2{2}'.format(optic, top_stage, dof)))
        opticalign_fm.append(ezca.get_LIGOFilter('SUS-{0}_{1}_OPTICALIGN_{2}'.format(optic, top_stage, dof)))

    # record initial state
    lock_gain = []
    lock_tramp = []
    opticalign_tramp = []

    for index in range(numOffload):
        lock_gain.append(lock_fm[index].GAIN.get())
        lock_tramp.append(lock_fm[index].TRAMP.get())
        opticalign_tramp.append(opticalign_fm[index].TRAMP.get())

    # simultaneously ramp LOCK gain to zero, and
    doList = []
    for index in range(numOffload):
        desired_bias = opticalign_fm[index].OFFSET.get()
        desired_bias += round((drivealign_fm[index].OUTPUT.get()/opticalign_fm[index].GAIN.get()), 4)
        doList.append([('off', lock_fm[index], 'INPUT'), ('ramp_gain', lock_fm[index], 0, ramptime), \
          ('ramp_offset', opticalign_fm[index], desired_bias, ramptime)])
    fez.do_many(ezca, doList)

    # wait for all ramps to complete (should use a timer for this)
    #for index in range(numOffload):
    #    while lock_fm[index].is_gain_ramping() or opticalign_fm[index].is_offset_ramping():
    #        log('waiting for ramps to finish')
    #        time.sleep(0.5)
    log('waiting for ramps to finish...')
    fez.wait_many(ezca, lock_fm + opticalign_fm)
    log('done waiting')

    # restore initial state
    doList = []
    for index in range(numOffload):
        doList.append([('clear', lock_fm[index]), ('on', lock_fm[index], 'INPUT'), \
          ('ramp_gain', lock_fm[index], lock_gain[index], lock_tramp[index]), \
          ('write', opticalign_fm[index].filter_name + '_TRAMP', opticalign_tramp[index])])
    fez.do_many(ezca, doList)

    
def offload(optic, stage):
    dofs = ['P','Y']

    control_chans = []
    offset_chans = []
    gain_chans = []
    time_ramp_chans = []


    if optic[:3]==('ETM' or 'ITM'):
        top_stage='M0'
    else:
        top_stage='M1'

    for jj in range(len(dofs)):

        control_chans.append('SUS-{0}_{1}_DRIVEALIGN_{2}_OUTMON'.format(optic, stage, dofs[jj]))
        offset_chans.append('SUS-{0}_{1}_OPTICALIGN_{2}_OFFSET'.format(optic, top_stage, dofs[jj]))
        gain_chans.append('SUS-{0}_{1}_OPTICALIGN_{2}_GAIN'.format(optic, top_stage, dofs[jj]))
        time_ramp_chans.append('SUS-{0}_{1}_OPTICALIGN_{2}_TRAMP'.format(optic, top_stage, dofs[jj]))
    drives = numpy.zeros((len(control_chans)))
    gains = numpy.zeros((len(control_chans)))
    old_sliders = numpy.empty((len(control_chans)))
    old_ramps = numpy.empty((len(control_chans)))
    new_sliders = numpy.empty((len(control_chans)))
    # We will use a series of loops over each channel to save time

    for j in range(len(dofs)):

        # Get the old slider valuesfor 
        old_sliders[j] = ezca[offset_chans[j]]

        # Get the slider gain settings
        gains[j] = ezca[gain_chans[j]]

        # Get the old ramp times
        old_ramps[j] = ezca[time_ramp_chans[j]]


    # Get the DRIVEALIGN outputs, get the OPTICALIGN gains, and add the outputs
    # to the alignment sliders

    for j in range(len(dofs)):
        # Get the DRIVEALIGN outputs; these will be offloaded to the OPTICALIGN sliders 
        #drives[j] += ezca[control_chans[j]]

        # Set the new slider values
        new_sliders[j] = old_sliders[j] + ezca[control_chans[j]] / gains[j]

        # Set the ramp time to 10 sec
        #ezca[time_ramp_chans[j]] = ramp_time

        # Set the new slider values.
        # As long as the servos are on and this is done slowly,
        # they should correct themselves!
        ezca[offset_chans[j]] = new_sliders[j]

def omc_asc_integrators(toggleOn):
    filters = ['POS_X', 'POS_Y', 'ANG_X', 'ANG_Y']
    [ezca.switch('OMC-ASC_' + filt, 'FM2', toggleOn.upper()) for filt in filters]


def clear_asc_input_matrix():
    for jj in range(1, 28):
        for ii in range(1, 17):
            ezca['ASC-INMATRIX_P_%d_%d'%(ii,jj)] = 0 
            ezca['ASC-INMATRIX_Y_%d_%d'%(ii,jj)] = 0 


    
def switch_coil_drivers(optic, stage, newState):
    # only works for prm, pr2, srm
    eul2osemTramp = 8
    analogExtraSleep = 7
    path = '/opt/rtcds/userapps/release/isc/h1/scripts/sus/'

    ezca['SUS-' + optic + '_' + stage + '_EUL2OSEM_TRAMP'] = eul2osemTramp
    
    coils = ['UL', 'UR', 'LL', 'LR']
    for coil in coils:
        print coil
        # put in new matrix, load it, sleep until output is zero
        ezca.burtwb(path+ optic.lower() + '_' + stage.lower() +'_out_' + coil.lower() + '.snap')
        time.sleep(1) # for burt to work
        ezca['SUS-' + optic + '_' + stage + '_EUL2OSEM_LOAD_MATRIX'] = 1
        time.sleep(eul2osemTramp)
        ezca['SUS-' + optic + '_' + stage + '_COILOUTF_' + coil + '_RSET'] = 2
        time.sleep(analogExtraSleep)

        # switch coil driver state. High range = 2, intermed = 1, low noise = 3
        ezca['SUS-' + optic + '_BIO_' + stage + '_' + coil + '_STATEREQ'] = 1  # go to intermediate state
        time.sleep(1)
        ezca['SUS-' + optic + '_BIO_' + stage + '_' + coil + '_STATEREQ'] = newState
        time.sleep(1)

    # put back in regular matrix
    ezca.burtwb(path+ optic.lower() + '_' + stage.lower() +'_out_normal.snap')
    time.sleep(1) # for burt to work
    ezca['SUS-' + optic + '_' + stage + '_EUL2OSEM_LOAD_MATRIX'] = 1
    time.sleep(eul2osemTramp)


#################################################
# centering function using GigE camera
#################################################
def GigE_centering(actuation_name, sensor_name, target_value, rate_value): 
    # move actuation point
    ezca[actuation_name] += rate_value * (target_value - ezca[sensor_name])
    # return the distance to the target
    return ezca[sensor_name] - target_value
##################################################
# check for lock
##################################################
from guardian import GuardState, GuardStateDecorator

def MC_locked():
    trans_pd_lock_threshold = 50
    return ezca['IMC-MC2_TRANS_SUM_OUTPUT']/ezca['IMC-PWR_IN_OUTPUT'] >= trans_pd_lock_threshold


def DRMI_locked():
    MichMon = ezca['LSC-MICH_TRIG_MON']
    PrclMon = ezca['LSC-PRCL_TRIG_MON']
    SrclMon = ezca['LSC-SRCL_TRIG_MON']
    if (MichMon > 0.5) and (PrclMon > 0.5) and (SrclMon > 0.5):
        # We're still locked and triggered, so return True
        return True
    else: 
        # Eeep!  Not locked.  Log some stuff
        log('DRMI TRIGGERED NOT LOCKED:')
        log('LSC-MICH_TRIG_MON = %s' % MichMon)
        log('LSC-PRCL_TRIG_MON = %s' % PrclMon)
        log('LSC-SRCL_TRIG_MON = %s' % SrclMon)
        return False
    #return ezca['LSC-MICH_TRIG_MON'] and ezca['LSC-PRCL_TRIG_MON'] and ezca['LSC-SRCL_TRIG_MON']

def PRMI_locked():
    MichMon = ezca['LSC-MICH_TRIG_MON']
    PrclMon = ezca['LSC-PRCL_TRIG_MON']
    if (ezca['LSC-MICH_TRIG_MON'] and ezca['LSC-PRCL_TRIG_MON']):
        log('PRMI locked')
        return True
    else:
        log('PRMI not locked')
        log('LSC-MICH_TRIG_MON = %s' % MichMon)
        log('LSC-PRCL_TRIG_MON = %s' % PrclMon)
        return False

def ARM_IR_locked(arm):
    if ezca['LSC-%sARM_FM_TRIG_MON'%arm]:
        #log('arm IR locked')
        return True
    else:
        #log('arm not IR locked')
        return False

# PRXY lock checker
def PRXY_locked():
    log('checking PR lock')
    avg = cdsutils.avg(1, 'LSC-ASAIR_A_LF_NORM_MON')
    log(avg)
    return cdsutils.avg(1, 'LSC-ASAIR_A_LF_NORM_MON') > lscparams.prxy_asair_lock_threshold

def PRXY_oscillating():
    return cdsutils.avg(1, 'LSC-POPAIR_A_LF_OUT_DQ', True)[1] >= lscparams.prxy_popairdc_oscillating_thresh

def arm_quiet():
    return ((cdsutils.avg(1,'ALS-C_TRX_A_LF_OUT_DQ', True)[1] <= alsconst.arm_quiet_thresh)
             and (cdsutils.avg(1,'ALS-C_TRY_A_LF_OUT_DQ', True)[1] <= alsconst.arm_quiet_thresh)
                and ezca['ALS-C_TRX_A_LF_OUTMON'] >= 0.850 and ezca['ALS-C_TRY_A_LF_OUTMON'] >= 0.90) # Decreased X Arm Threshold to 0.85 
                                                                                                      # because of slowly reducing ALS X laser power during O2, 
                                                                                                      # JSK, JD, CG 2016-12-19

def oplevs_quiet():
    return ((cdsutils.avg(1,'SUS-ETMY_L3_OPLEV_YAW_OUT_DQ', True)[1] <= 0.5)
             and (cdsutils.avg(1,'SUS-ETMY_L3_OPLEV_YAW_OUT_DQ', True)[1] <= 0.5))

def SRXY_locked():
    return ezca['LSC-SRCL_TRIG_MON'] >= 1

def SRM_sus_saturating():
    # Check the 9th bit of the FEC state word for SRM front end,
    # which indicates overflows in the DAC outputs.
    return int(ezca['FEC-45_STATE_WORD']) & 1 << 9

def SR2_sus_saturating():
    # Check the 9th bit of the FEC state word for SRM front end,
    # which indicates overflows in the DAC outputs
    return int(ezca['FEC-41_STATE_WORD']) & 1 << 9

def MICHDARK_locked():
    return cdsutils.avg(1, 'LSC-ASAIR_A_LF_NORM_MON') <= lscparams.michdark_locked_threshold

def MICHBRIGHT_locked():
    return cdsutils.avg(1, 'LSC-ASAIR_A_LF_NORM_MON') >= lscparams.michbright_locked_threshold

def is_locked(dof):
    if dof == 'YARM_GREEN':
        return True   
    if dof == 'XARM_GREEN':
        return True
    if dof == 'YARM':
        return ARM_IR_locked('Y')    
    if dof == 'XARM':
        return ARM_IR_locked('X')
    if dof == 'PRX':
        return PRXY_locked()
    if dof == 'PRY':
        return PRXY_locked()
    if dof == 'SRX':
        return PRXY_locked()
    if dof == 'SRY':
        return SRXY_locked()
    if dof == 'PRMI':
        return PRMI_locked()
    if dof == 'MICH_DARK':
        return MICHDARK_locked()
    if dof == 'DRMI':
        return DRMI_locked()

#################################################
# check for error conditions
#################################################
def DRMI_aligned():
        if not ezca.read('GRD-SUS_PRM_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_PR2_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_PR3_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_SRM_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_SR2_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_SR3_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_ITMY_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_ITMX_STATE_S', as_string=True) == 'ALIGNED':
            return False        
        #elif not ezca.read('GRD-SUS_ETMY_STATE_S', as_string=True) == 'MISALIGNED':
        #    return False
        #elif not ezca.read('GRD-SUS_ETMX_STATE_S', as_string=True) == 'MISALIGNED':
        #    return False
        elif not ezca.read('GRD-SUS_BS_STATE_S', as_string=True) == 'ALIGNED':
            return False
        else:
            return True

def BS_opLev_OK(lscparams):
    oplev_y_avg = cdsutils.avg(1, 'SUS-BS_M3_OPLEV_YAW_OUTPUT', True)  # this needs to be changed, why should the dc value indicate an oscillation?  should this be the stdev?
    oplev_p_avg = cdsutils.avg(1, 'SUS-BS_M3_OPLEV_PIT_OUTPUT', True)
    if oplev_p_avg == lscparams.bs_oscillation_thresh and oplev_y_avg == lscparams.bs_oscillation_thresh:
        # equality, AND statement to keep this from happening
        notify('BS oscillation WHYWHYWHY')
        return True
    else:
        return True

def input_matrix_OK():
    flag = 1
    smallNum = 1e-2
    numRows = 8
    numCols = 29
    wait_msg = 'Waiting for input matrix elements ';
    for ii in range(1, numRows):
        for jj in range(1, numCols):
            matrixValue = ezca['LSC-PD_DOF_MTRX_{}_{}'.format(ii,jj)]
            settingValue = ezca['LSC-PD_DOF_MTRX_SETTING_{}_{}'.format(ii,jj)]
            if not abs(matrixValue-settingValue) / (abs(settingValue)+smallNum) < smallNum:
                wait_msg += '({0}, {1}) '.format(ii, jj)
                flag = 0
    if not flag:
        notify(wait_msg)
    return flag

def rampMatrixOk(matrixName, chansList):
    """
    matrixName is something like 'LSC-PD_DOF_MTRX'

    chansList is a list of ordered pairs corresponding to channel names; e.g.,
    [(2, 4), (3, 13)]
    """
    matrixGood = True
    smallNum = 1e-2
    for pair in chansList:
        matrixVal = ezca[matrixName+'_{0}_{1}'.format(*pair)]
        settingVal = ezca[matrixName+'_SETTING_{0}_{1}'.format(*pair)]
        if not (abs(matrixVal - settingVal) / (abs(settingVal) + smallNum) < smallNum):
            matrixGood = False
    return matrixGood

def loadRampMatrix(matrixName, chansList, numTries):
    """
    matrixName is something like 'LSC-PD_DOF_MTRX'

    chansList is a list of ordered pairs corresponding to channel names;
    e.g., [(2, 4), (3, 13)]
    """
    rampTime = ezca[matrixName+'_TRAMP']
    ezca[matrixName+'_LOAD_MATRIX'] = 1
    time.sleep(rampTime+1)
    ii = 0
    while ii < numTries:
        matrixGood = rampMatrixOk(matrixName, chansList)
        if matrixGood:
            break
        else:
            notify("Pushing the 'load matrix' button again...")
            ezca[matrixName+'_LOAD_MATRIX'] = 1
            time.sleep(rampTime+1)
        ii += 1
    return matrixGood
        

def ASC_DC_centering_servos_OK():
    if ezca['ASC-WFS_GAIN'] < 0.1:
        notify('ASC MASTER GAIN')
    return not ((abs(ezca['ASC-DC1_P_OUTPUT']) >=  ezca['ASC-DC1_P_LIMIT']
                 or abs(ezca['ASC-DC1_Y_OUTPUT']) >=  ezca['ASC-DC1_Y_LIMIT']
                 or abs(ezca['ASC-DC2_P_OUTPUT']) >=  ezca['ASC-DC2_P_LIMIT']
                 or abs(ezca['ASC-DC2_Y_OUTPUT']) >=  ezca['ASC-DC2_Y_LIMIT']))

def CORNER_WFS_DC_centering_servos_OK():
    if ezca['ASC-WFS_GAIN'] < 0.1:
        notify('ASC MASTER GAIN')
    return not ((abs(ezca['ASC-DC1_P_OUTPUT']) >= ezca['ASC-DC1_P_LIMIT']
                 or abs(ezca['ASC-DC1_Y_OUTPUT']) >= ezca['ASC-DC1_Y_LIMIT']
                 or abs(ezca['ASC-DC2_P_OUTPUT']) >= ezca['ASC-DC2_P_LIMIT']
                 or abs(ezca['ASC-DC2_Y_OUTPUT']) >= ezca['ASC-DC2_Y_LIMIT']
                 or abs(ezca['ASC-DC3_P_OUTPUT']) >= ezca['ASC-DC3_P_LIMIT']
                 or abs(ezca['ASC-DC3_Y_OUTPUT']) >= ezca['ASC-DC3_Y_LIMIT']
                 or abs(ezca['ASC-DC4_P_OUTPUT']) >= ezca['ASC-DC4_P_LIMIT']
                 or abs(ezca['ASC-DC4_Y_OUTPUT']) >= ezca['ASC-DC4_Y_LIMIT']))

def REFL_WFS_DC_centering_servos_OK():
    if ezca['ASC-WFS_GAIN'] < 0.1:
        notify('ASC MASTER GAIN')
    return not ((abs(ezca['ASC-DC1_P_OUTPUT']) >= ezca['ASC-DC1_P_LIMIT']
                 or abs(ezca['ASC-DC1_Y_OUTPUT']) >= ezca['ASC-DC1_Y_LIMIT']
                 or abs(ezca['ASC-DC2_P_OUTPUT']) >= ezca['ASC-DC2_P_LIMIT']
                 or abs(ezca['ASC-DC2_Y_OUTPUT']) >= ezca['ASC-DC2_Y_LIMIT']))

def AS_WFS_DC_centering_servos_OK():
    if ezca['ASC-WFS_GAIN'] < 0.1:
        notify('ASC MASTER GAIN')
    return not ((abs(ezca['ASC-DC3_P_OUTPUT']) >= ezca['ASC-DC3_P_LIMIT']
                 or abs(ezca['ASC-DC3_Y_OUTPUT']) >= ezca['ASC-DC3_Y_LIMIT']
                 or abs(ezca['ASC-DC4_P_OUTPUT']) >= ezca['ASC-DC4_P_LIMIT']
                 or abs(ezca['ASC-DC4_Y_OUTPUT']) >= ezca['ASC-DC4_Y_LIMIT']))

def ezcaAverageMultiple(signals, dt=2, rate=0.05):
    """
    Average ezca data over multiple reads.

    This function is used to check the WFS centering.
    """
    # signals = list of signals
    # dt = total average time
    # rate = time beteween each subsequent sampling
    N = int(dt / rate)
    sig = numpy.zeros(len(signals))
    for i in range(N):
        for j in range(len(signals)):
            sig[j] = sig[j] + ezca[signals[j]]
        time.sleep(rate)
    sig = sig/N
    return sig

def REFL_PD_OK():
    return ezca['LSC-REFL_A_LF_OUTPUT'] <= 100

def ready_for_locking():
    message = []
    flag = False

    # check input matrix (using REFLA RF 45 I) 
    # (this is needed because of bug in ramping matrix)
    #log('checking input matrix')
    #if not input_matrix_OK():
    #    flag = True
    #    message.append('Check LSC input matrix')

    # checking the whole matrix takes forever, let's just click load and go on
    # the "load button doesn't work" bug was fixed, right?
    #ezca['LSC-PD_DOF_MTRX_LOAD_MATRIX'] = 1

    #check that LSC feedback is enabled
    if not ezca.read('LSC-CONTROL_ENABLE'):
        flag = True
        message.append('LSC CONTROL disabled')
    if not ASC_DC_centering_servos_OK():
        flag = True
        message.append('Check DC centering servos') 
    if not REFL_PD_OK():
        flag = True
        message.append('No light on refl PD')       
    if flag:
        notify(', '.join(message))
        return False
    else:
        return True

def ready_for_locking_MICH():
    message = []
    flag = False
    #check input matrix (using REFLA RF 45 I) 
    #(this is needed because of bug in ramping matrix)
    if not input_matrix_OK():
        flag = True
        message.append('Check LSC input matrix')
    #check that LSC feedback is enabled
    if not ezca.read('LSC-CONTROL_ENABLE'):
        flag = True
        message.append('LSC CONTROL disabled')       
    if flag:
        notify(', '.join(message))
        return False
    else:
        return True

def ready_for_PRXY(lscparams, arm):
    message = []
    flag = False
    #check input matrix (using REFLA RF 45 I) 
    #(this is needed because of bug in ramping matrix)
    if not input_matrix_OK():
        flag = True
        message.append('check LSC input matrix')
    #check that LSC feedback is enabled
    if not ezca.read('LSC-CONTROL_ENABLE'):
        flag = True
        message.append('LSC CONTROL disabled')
    if not ASC_DC_centering_servos_OK():
        flag = True
        message.append('Refl PD, check DC centering servos')      
    if flag:
        notify(', '.join(message))
        return False
    else:
        return True

def ready_for_PRMI(lscparams):
    message = []
    flag = False
    if not input_matrix_OK():
        log('checking input matrix')
        flag = True
        message.append('Check input matrix')
    #check that LSC feedback is enabled
    if not ezca.read('LSC-CONTROL_ENABLE'):
        flag = True
        message.append('LSC CONTROL disabled') 
    if ((not ezca['LSC-POPAIR_B_RF18_WHITEN_GAIN'] == 12)
          or not ezca.get_LIGOFilter('LSC-POPAIR_B_RF18_I').is_engaged('FM9')
          or not ezca.get_LIGOFilter('LSC-POPAIR_B_RF18_I').is_engaged('FM10')
          or not ezca.get_LIGOFilter('LSC-POPAIR_B_RF18_Q').is_engaged('FM9')
          or not ezca.get_LIGOFilter('LSC-POPAIR_B_RF18_Q').is_engaged('FM10')):
        flag = True
        message.append('Check POPAIR_B_RF18 whitening filters')
    if not BS_opLev_OK(lscparams): 
        flag = True
        message.append('BS OpLev oscillating')
    if flag:
        notify(', '.join(message))
        return False
    else:
        return True

# checks the PSL input power
def PSLinputpower_OK():
    message = []
    flag = False
    if ezca['PSL-PERISCOPE_A_DC_POWERMON'] > 11000.0:
        flag = True
        message.append('PSL input power above 11W')
    elif (ezca['PSL-PERISCOPE_A_DC_POWERMON'] < 9000.0
            and ezca['PSL-ROTATIONSTAGE_POWER_REQUEST'] == 9.0):
        flag = True
        message.append('PSL input power below 9W, request 10W')
    elif (ezca['PSL-PERISCOPE_A_DC_POWERMON'] > 6000.0
            and ezca['PSL-ROTATIONSTAGE_POWER_REQUEST'] == 4.0):
        flag = True
        message.append('PSL input power above 6W, request 4W')
    elif (ezca['PSL-PERISCOPE_A_DC_POWERMON'] < 2000.0
            and ezca['PSL-ROTATIONSTAGE_POWER_REQUEST'] == 4.0):
        flag = True
        message.append('PSL input power below 2W, request 4W')
    #log(flag)    
    if flag:
        notify(', '.join(message))
        return False
    else:
        return True

#################################################
# DECORATORS
#################################################
def node_watchdog_tripped(node):
    return 'WATCHDOG_TRIPPED' in node.state

def watchdog_tripped_nodes(nodes):
    """Return list of nodes that are in tripped state."""
    tripped_nodes = []
    for node in nodes:
        if node_watchdog_tripped(node):
            tripped_nodes.append(node)
    return tripped_nodes

def get_subordinate_watchdog_check_decorator(nodes):
    class subordinate_watchdog_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""
        def pre_exec(self):
            if watchdog_tripped_nodes(nodes):
                #return 'WATCHDOG_TRIPPED_SUBORDINATE'
                notify('WATCHDOG TRIPPED')
    return subordinate_watchdog_check

def unstall_nodes(nodes):
    class unstall_decorator(GuardStateDecorator):
        def pre_exec(self):
            for node in nodes.get_stalled_nodes():
                # put a check that in is in done state
                if not node.NOTIFICATION:
                    log('Unstalling ' + node.name)
                    node.revive()
    return unstall_decorator

def get_watchdog_IMC_check_decorator(nodes):
    class watchdog_IMC_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""
        def pre_exec(self):
            if watchdog_tripped_nodes(nodes):
                notify('WATCHDOG TRIPPED')
            if not MC_locked():
                log('MC not locked')
                return 'LOCKLOSS'
    return watchdog_IMC_check

def get_watchdog_IMC_ARMS_check_decorator(nodes):
    class watchdog_IMC_ARMS_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""
        def pre_exec(self):
            if watchdog_tripped_nodes(nodes):
                notify('WATCHDOG TRIPPED')
            if not MC_locked():
                log('MC not locked')
                return 'LOCKLOSS'
            if (nodes['ALS_XARM'] in ['UNLOCKED', 'LOCKING', 'FAULT']
                  or nodes['ALS_YARM'] in ['UNLOCKED', 'LOCKING', 'FAULT']):
                log('arms not locked with green')
                return 'LOCKLOSS'
    return watchdog_IMC_ARMS_check

def get_watchdog_IMC_ARMS_DRMI_check_decorator(nodes):
    class watchdog_IMC_ARMS_DRMI_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""
        def pre_exec(self):
            if watchdog_tripped_nodes(nodes):
                notify('WATCHDOG TRIPPED')
            if not MC_locked():
                log('MC not locked')
                return 'LOCKLOSS'
            if (nodes['ALS_XARM'] in ['UNLOCKED', 'LOCKING', 'FAULT']
                  or nodes['ALS_YARM'] in ['UNLOCKED', 'LOCKING', 'FAULT']):
                log('arms not locked with green')
                return 'LOCKLOSS'
            if not DRMI_locked():
                log('DRMI lost lock')
                return 'LOCKLOSS_DRMI'
    return watchdog_IMC_ARMS_DRMI_check

def get_watchdog_IMC_DRMI_check_decorator(nodes):
    class watchdog_IMC_DRMI_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""
        def pre_exec(self):
            if watchdog_tripped_nodes(nodes):
                #return 'WATCHDOG_TRIPPED_SUBORDINATE'
                notify('WATCHDOG TRIPPED')
            if not MC_locked():
                log('MC not locked')
                return 'LOCKLOSS'
            if not DRMI_locked():
                log('DRMI lost lock')
                return 'LOCKLOSS_DRMI'
            if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                notify('Fast shutter closed (Toast is ready!!!!) turning off WFS')
                ezca.switch('ASC-DHARD_P', 'INPUT', 'OFF')
                ezca.switch('ASC-DHARD_Y', 'INPUT', 'OFF')
                
    return watchdog_IMC_DRMI_check


class assert_mc_locked(GuardStateDecorator):
    def pre_exec(self):
        if not MC_locked():
            log('MC not Locked')
            return 'DOWN'

class assert_drmi_locked(GuardStateDecorator):
    def pre_exec(self):
        if not DRMI_locked():
            log('DRMI not Locked')
            return 'LOCK_DRMI_1F' 

####used by ISC_configs only
class assert_xarm_IR_locked(GuardStateDecorator):
    def pre_exec(self):
        if not ARM_IR_locked('X'):
            log('arm not Locked')
            return 'DOWN'

class assert_yarm_IR_locked(GuardStateDecorator):
    def pre_exec(self):
        if not ARM_IR_locked('Y'):
            log('arm not Locked')
            return 'DOWN'

class assert_arm_IR_locked(GuardStateDecorator):
    def pre_exec(self):
        if not (ARM_IR_locked('X') or ARM_IR_locked('Y')):
            log('arm not Locked')
            return 'DOWN'


class assert_michdark_locked(GuardStateDecorator):
    def pre_exec(self):
        if not MICHDARK_locked():
            return 'DOWN'


class assert_michbright_locked(GuardStateDecorator):
    def pre_exec(self):
        if not MICHBRIGHT_locked():
            return 'LOCK_MICH_BRIGHT'

class assert_prmisb_locked(GuardStateDecorator):
    def pre_exec(self):
        if not PRMI_locked():
            return 'LOCK_PRMI_SB'

class assert_prx_locked(GuardStateDecorator):
    def pre_exec(self):
        if not PRXY_locked():
            return 'DOWN'

class assert_sry_locked(GuardStateDecorator):
    def pre_exec(self):
        if not SRXY_locked():
            return 'DOWN'

def assert_dof_locked_gen(dof):
    class assert_dof_locked(GuardStateDecorator):
        def pre_exec(self):
            if not is_locked(dof):
                if dof == 'DRMI':
                    log('la la')
                    return 'LOCK_DRMI_1F'
                else:
                    log('ta ta')
                    return 'DOWN'
    return assert_dof_locked

#Example: @FM_limit_checker_decorator(['ASC-PRC1_P','ASC-PRC1_Y'],-1.0,['INPUT','FM2'],['ON','ON'],['OFF','OFF'])
def FM_limit_checker_decorator(FMlist,sign,action,onstate,offstate):
    class FM_limit_checker(GuardStateDecorator):
        """Check for FM reaching limiters - if so, turn off input"""
        def pre_exec(self):
            #if not(type(FMlist) is list):                   # make sure FMlist is a lists
            #    FMlist=[FMlist]
            for FM in FMlist:
                if 'OFFSET' in ezca.switch(FM):                 # if the OFFSET is on, we'll have to add OFFSET to the input
                    offsetValue = ezca[FM+'_OFFSET']
                else:
                    offsetValue = 0
                #
                #if not(type(action) is list):                   # make sure all arugments are lists
                #    action  =[action]
                #    onstate =[onstate]
                #    offstate=[offstate]
                #
                if  ((-ezca[FM+'_OUTMON'] >= ezca[FM+'_LIMIT']) and (-(ezca[FM+'_INMON']+offsetValue)*sign>= 0)) or\
                    (( ezca[FM+'_OUTMON'] >= ezca[FM+'_LIMIT']) and ( (ezca[FM+'_INMON']+offsetValue)*sign>= 0)) :
                    for idx, act in enumerate(action):
                        if action[idx] in ezca.switch(FM):                    # If the filter is 'ON'
                            if offstate[idx] != 'ON':                         # If offstate is not equal to 'ON'
                                ezca.switch(FM, action[idx], offstate[idx])   # Switch the state
                        else:                                                 # If the filter is not 'ON'
                            if offstate[idx] != 'OFF':                        # If offstate is not equal to 'OFF'
                                ezca.switch(FM, action[idx], offstate[idx])   # Switch to requested state
                else:
                    for idx, act in enumerate(action):                                        
                        if action[idx] in ezca.switch(FM):                    # Same logic as above for onstate
                            if onstate[idx] != 'ON':
                                ezca.switch(FM, action[idx], onstate[idx])   
                        else:
                            if onstate[idx] != 'OFF':
                                ezca.switch(FM, action[idx], onstate[idx])
    return FM_limit_checker

def adjustDamperGain(FM,maxgain,targetCounts):
    """read the FM output and scale the gain to match targetCOunts"""
    dmax=numpy.max(numpy.abs(cdsutils.getdata(FM+'_OUTPUT',1).data))
    oldgain=ezca[FM+'_GAIN']
    newgain=round(oldgain*targetCounts/dmax, 4)
    if (abs(newgain/maxgain)<1.0):
        ezca[FM+'_GAIN'] = newgain
    else:
        ezca[FM+'_GAIN'] = maxgain
    
def lockISS():
    ezca['PSL-ISS_SECONDLOOP_CLOSED'] = 0 # make sure nothing going to IFO
    time.sleep(0.1)
    ezca['PSL-ISS_SECONDLOOP_TRIG_THRESH_ON'] = 100
    ezca['PSL-ISS_SECONDLOOP_PD_SW'] = 0
    ezca['PSL-ISS_SECONDLOOP_ADD_PD_58_SUM'] = 0
    ezca['PSL-ISS_SECONDLOOP_PD_SW'] = 0
    ezca['PSL-ISS_SECONDLOOP_ADD_PD_58_SUM'] = 0
    ezca['PSL-ISS_SECONDLOOP_SERVO_ON']=32000  # make sure "servo" is closed, so while loop works
    while abs(ezca['PSL-ISS_SECONDLOOP_SIGNAL_OUTPUT'])>0.025:
        ezca['PSL-ISS_SECONDLOOP_REF_SIGNAL_ANA']=ezca['PSL-ISS_SECONDLOOP_REF_SIGNAL_ANA'] +  ezca['PSL-ISS_SECONDLOOP_SIGNAL_OUTPUT']/700
        time.sleep(10)
    ezca['PSL-ISS_SECONDLOOP_CLOSED'] = 32000
    time.sleep(0.5)

##################################################
# Functions to aid with in-lock alignment        #
##################################################
def alignRef():
    class alignRefClass:
        ppr3=ezca['SUS-PR3_M1_OPTICALIGN_P_OFFSET']
        ppr2=ezca['SUS-PR2_M1_OPTICALIGN_P_OFFSET']
        pprm=ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET']
        pim4=ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET']
        pim3=ezca['SUS-IM3_M1_OPTICALIGN_P_OFFSET']
        psr2=ezca['SUS-SR2_M1_OPTICALIGN_P_OFFSET']
        psrm=ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET']
        pex =ezca['SUS-ETMX_M0_OPTICALIGN_P_OFFSET']
        pey =ezca['SUS-ETMY_M0_OPTICALIGN_P_OFFSET']
        pix =ezca['SUS-ITMX_M0_OPTICALIGN_P_OFFSET']
        piy =ezca['SUS-ITMY_M0_OPTICALIGN_P_OFFSET']
        ypr3=ezca['SUS-PR3_M1_OPTICALIGN_Y_OFFSET']
        ypr2=ezca['SUS-PR2_M1_OPTICALIGN_Y_OFFSET']
        yprm=ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET']
        yim4=ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET']
        yim3=ezca['SUS-IM3_M1_OPTICALIGN_Y_OFFSET']
        ysr2=ezca['SUS-SR2_M1_OPTICALIGN_Y_OFFSET']
        ysrm=ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET']
        yex =ezca['SUS-ETMX_M0_OPTICALIGN_Y_OFFSET']
        yey =ezca['SUS-ETMY_M0_OPTICALIGN_Y_OFFSET']
        yix =ezca['SUS-ITMX_M0_OPTICALIGN_Y_OFFSET']
        yiy =ezca['SUS-ITMY_M0_OPTICALIGN_Y_OFFSET']
    return alignRefClass()

def pr3spotmove(ref):
    ###########################
    # alog 28764
    ###########################

    # set 2
    #gppr3=0.086
    #gpprm=-13.07
    #gpim4=-305
    #gpsr2=-2.73
    #gpsrm=4.29
    #gpex=-0.7
    #gpey=-0.7
    #gpix=-0.45
    #gpiy=-0.35
    # set 1
    gppr3=0.07
    gpprm=-13.2
    gpim4=-325
    gpsr2=-2.39
    gpsrm=3.2
    gpex=-0.7
    gpey=-0.7
    gpix=-0.45
    gpiy=-0.35
    # set 1 yaw
    gypr3=0.028
    gyprm=11.07
    gyim4=61.22
    gysr2=2.22
    gysrm=2.74
    gyex=-0.5
    gyey=+0.5
    gyix=+0.32
    gyiy=-0.31
    #
    rpprm=ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'];
    gp=1.0/gpprm*(rpprm-ref.pprm)
    ezca['SUS-PR3_M1_OPTICALIGN_P_OFFSET']  = ref.ppr3 + gppr3*gp
    #ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET']  = ref.pprm + gpprm*gp
    ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET']  = ref.pim4 + gpim4*gp
    ezca['SUS-SR2_M1_OPTICALIGN_P_OFFSET']  = ref.psr2 + gpsr2*gp
    ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET']  = ref.psrm + gpsrm*gp
    ezca['SUS-ETMX_M0_OPTICALIGN_P_OFFSET'] = ref.pex  + gpex *gp
    ezca['SUS-ETMY_M0_OPTICALIGN_P_OFFSET'] = ref.pey  + gpey *gp
    ezca['SUS-ITMX_M0_OPTICALIGN_P_OFFSET'] = ref.pix  + gpix *gp
    ezca['SUS-ITMY_M0_OPTICALIGN_P_OFFSET'] = ref.piy  + gpiy *gp
    #
    ryprm=ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET'];
    gy=1.0/gyprm*(ryprm-ref.yprm)
    ezca['SUS-PR3_M1_OPTICALIGN_Y_OFFSET']  = ref.ypr3 + gypr3*gy
    #ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET']  = ref.yprm + gyprm*gy
    ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET']  = ref.yim4 + gyim4*gy
    ezca['SUS-SR2_M1_OPTICALIGN_Y_OFFSET']  = ref.ysr2 + gysr2*gy
    ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET']  = ref.ysrm + gysrm*gy
    ezca['SUS-ETMX_M0_OPTICALIGN_Y_OFFSET'] = ref.yex  + gyex *gy
    ezca['SUS-ETMY_M0_OPTICALIGN_Y_OFFSET'] = ref.yey  + gyey *gy
    ezca['SUS-ITMX_M0_OPTICALIGN_Y_OFFSET'] = ref.yix  + gyix *gy
    ezca['SUS-ITMY_M0_OPTICALIGN_Y_OFFSET'] = ref.yiy  + gyiy *gy
    #
    return True

def pr2spotmove(ref):
    pitPR3toPR2=-9.2;
    yawPR3toPR2=+9.2;
    pitPR3toIM4=56;
    yawPR3toIM4=11;
    pitPR3toPRM=1.5;
    yawPR3toPRM=2.2;
    #
    p3=ezca['SUS-PR3_M1_OPTICALIGN_P_OFFSET']
    y3=ezca['SUS-PR3_M1_OPTICALIGN_Y_OFFSET']
    ezca['SUS-PR2_M1_OPTICALIGN_P_OFFSET'] = ref.ppr2 + pitPR3toPR2*(p3-ref.ppr3)
    ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET'] = ref.pim4 + pitPR3toIM4*(p3-ref.ppr3)
    ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'] = ref.pprm + pitPR3toPRM*(p3-ref.ppr3)
    ezca['SUS-PR2_M1_OPTICALIGN_Y_OFFSET'] = ref.ypr2 + yawPR3toPR2*(y3-ref.ypr3)
    ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET'] = ref.yim4 + yawPR3toIM4*(y3-ref.ypr3)
    ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET'] = ref.yprm + yawPR3toPRM*(y3-ref.ypr3)
    return True

def prmspotmove(ref):
    pitIM3toPR2=+0.0022;
    yawIM3toPR2=+0.0055;
    pitIM3toIM4=-1.3;
    yawIM3toIM4=+0.92;
    pitIM3toPRM=-0.009;
    yawIM3toPRM=+0.031;
    #
    pi3=ezca['SUS-IM3_M1_OPTICALIGN_P_OFFSET'];
    yi3=ezca['SUS-IM3_M1_OPTICALIGN_Y_OFFSET'];
    ezca['SUS-PR2_M1_OPTICALIGN_P_OFFSET'] = ref.ppr2 + pitIM3toPR2*(pi3-ref.pim3)
    ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET'] = ref.pim4 + pitIM3toIM4*(pi3-ref.pim3)
    ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'] = ref.pprm + pitIM3toPRM*(pi3-ref.pim3)
    ezca['SUS-PR2_M1_OPTICALIGN_Y_OFFSET'] = ref.ypr2 + yawIM3toPR2*(yi3-ref.yim3)
    ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET'] = ref.yim4 + yawIM3toIM4*(yi3-ref.yim3)
    ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET'] = ref.yprm + yawIM3toPRM*(yi3-ref.yim3)
    return True


##################################################
# LSC/ASC input and out matrices                 #
##################################################
# LSC input matrix for the majority
intrix = cdsutils.CDSMatrix('LSC-PD_DOF_MTRX',
		ramping = True,
		cols={'POP_A9I':1,
			'POP_A9Q':2,
			'POP_A45I':3,
			'POP_A45Q':4,
			'REFL_A9I':5,
			'REFL_A9Q':6,
			'REFL_A45I':7,
			'REFL_A45Q':8,
			'POPAIR_A9I':9,
			'POPAIR_A9Q':10,
			'POPAIR_A45I':11,
			'POPAIR_A45Q':12,
			'REFLAIR_A9I':13,
			'REFLAIR_A9Q':14,
			'REFLAIR_A45I':15,
			'REFLAIR_A45Q':16,
			'REFLAIR_B27I':17,
			'REFLAIR_B27Q':18,
			'REFLAIR_B135I':19,
			'REFLAIR_B135Q':20,
			'TRX':21,
			'TRY':22,
			'REFLSERVO_SLOW': 23,
			'ALS_COMM': 24,
			'ASAIR_ALF': 25,
			'TR_CARM':26,
			'TR_REFL9':27,
			'REFL_DC':28,
			'OMC_DC':29,
			'ASAIR_A45I':30,
			'ASAIR_A45Q':31
			},
		rows={'DARM':1,
			'CARM':2,
			'MICH':3,
			'PRCL':4,
			'SRCL':5,
			'MCL':6,
			'XARM':7,
			'YARM':8,
			'REFLBIAS':9
			}
		)

# LSC input matrix for a minority (i.e. OMC/AS45 to DARM and CARM)
intrix_OMCAS45 = cdsutils.CDSMatrix('LSC-ARM_INPUT_MTRX',
		ramping = True,
		cols={'OMCDC':1,
			'ASAIR_A45I':2,
			'ASAIR_A45Q':3,
			'ALS_DIFF':4,
			'REFL_DC':5
			},
		rows={'DARM':1,
			'CARM':2
			}
		)


# LSC input matrix for a minority (i.e. OMC/AS45 to nonDARM or nonCARM)
intrix_ASPDs = cdsutils.CDSMatrix('LSC-ASPD_DOF_MTRX',
		ramping = True,
		cols={'OMCDC':1,
			'ASAIR_A45I':2,
			'ASAIR_A45Q':3,
			},
		rows={'MICH':1,
			'PRCL':2,
			'SRCL':3,
			'MCL':4,
			'XARM':5,
			'YARM':6
			}
		)

# LSC output matrix for the majority
outrix = cdsutils.CDSMatrix('LSC-OUTPUT_MTRX',
		cols={'MICH':1,
			'PRCL':2,
			'SRCL':3,
			'MCL':4,
			'XARM':5,
			'YARM':6,
			'OSC1':7,
			'OSC2':8,
			'OSC3':9,
			'MICHFF':10,
			'SRCLFF':11
			},
		rows={'ETMX':1,
			'ETMY':2,
			'ITMX':3,
			'ITMY':4,
			'PRM':5,
			'SRM':6,
			'BS':7,
			'PR2':8,
			'SR2':9,
			'MC2':10
			}
		)

# output matrix
darmcarm_outrix = cdsutils.CDSMatrix('LSC-ARM_OUTPUT_MTRX',
		cols={'DARM':1, 'CARM':2},
		rows={'ETMX':1, 'ETMY':2}
		)

# LSC trigger matrix
trigrix = cdsutils.CDSMatrix('LSC-TRIG_MTRX',
		cols={'OMCDC':1,
				'POPAIR_B_RF18_I':2,
				'POPAIR_B_RF18_Q':3,
				'POPAIR_B_RF90_I':4,
				'POPAIR_B_RF90_Q':5,
				'ASAIR_B_RF18_I':6,
				'ASAIR_B_RF18_Q':7,
				'ASAIR_B_RF90_I':8,
				'ASAIR_B_RF90_Q':9,
				'REFLAIR_A_DC':10,
				'REFLAIR_B_DC':11,
				'POPAIR_A_DC':12,
				'POPAIR_B_DC':13,
				'ASAIR_A_DC':14,
				'ASAIR_B_DC':15,
				'REFL_A_DC':16,
				'POP_A_DC':17,
				'TRX':18,
				'TRY':19
			},
			rows={'DARM':1,
			 	'MICH':2,
				'PRCL':3,
				'SRCL':4,
				'CARM':5,
				'XARM':6,
				'YARM':7,
				'REFLBIAS':7
				}
			)

# ASC input matrix
asc_rows = {'INP1':1,
					'INP2':2,
					'PRC1':3,
					'PRC2':4,
					'MICH':5,
					'SRC1':6,
					'SRC2':7,
					'DHARD':8,
					'DSOFT':9,
					'CHARD':10,
					'CSOFT':11,
					'DC1':12,
					'DC2':13,
					'DC3':14,
					'DC4':15,
					'DC5':16
					}
asc_columns ={'AS_A_RF45_I':1,
					'AS_A_RF45_Q':2,
					'AS_A_RF36_I':3,
					'AS_A_RF36_Q':4,
					'AS_B_RF45_I':5,
					'AS_B_RF45_Q':6,
					'AS_B_RF36_I':7,
					'AS_B_RF36_Q':8,
					'REFL_A_RF9_I':9,
					'REFL_A_RF9_Q':10,
					'REFL_A_RF45_I':11,
					'REFL_A_RF45_Q':12,
					'REFL_B_RF9_I':13,
					'REFL_B_RF9_Q':14,
					'REFL_B_RF45_I':15,
					'REFL_B_RF45_Q':16,
					'REFL_A_DC':17,
					'REFL_B_DC':18,
					'AS_A_DC':19,
					'AS_B_DC':20,
					'POP_A_DC':21,
					'POP_B_DC':22,
					'TRX_A':23,
					'TRX_B':24,
					'TRY_A':25,
					'TRY_B':26,
					'AS_C_DC':27, 
                    'IM4_TRANS':28,
                    'POP_X_I':29,
                    'POP_X_Q':30,
					}

# ASC input matrices 
asc_intrix_pit = cdsutils.CDSMatrix('ASC-INMATRIX_P',
		cols = asc_columns,
		rows = asc_rows
				)

asc_intrix_yaw = cdsutils.CDSMatrix('ASC-INMATRIX_Y',
		cols = asc_columns,
		rows = asc_rows
				)


asc_rows_extra = {'ALS_X1':17,
					'ALS_X2':18,
					'ALS_X3':19,
					'ALS_Y1':20,
					'ALS_Y2':21,
					'ALS_Y3':22,
					'OSC1':23,
					'OSC2':24,
					'OSC3':25
					}

asc_actuators = {'PRM':1,
				'PR2':2,
				'PR3':3,
				'BS':4,
				'ITMX':5,
				'ITMY':6,
				'ETMX':7,
				'ETMY':8,
				'SRM':9,
				'SR2':10,
				'SR3':11,
				'IM1':12,
				'IM2':13,
				'IM3':14,
				'IM4':15,
				'RM1':16,
				'RM2':17,
				'OM1':18,
				'OM2':19,
				'OM3':22, # careful for the location of OM3
				'TMSX':20,
				'TMSY':21
					}

asc_rows.update(asc_rows_extra) # append asc_rows_extra

# ASC output matrices
asc_outrix_pit = cdsutils.CDSMatrix('ASC-OUTMATRIX_P',
			cols=asc_rows,
			rows=asc_actuators )
asc_outrix_yaw = cdsutils.CDSMatrix('ASC-OUTMATRIX_Y',
			cols=asc_rows,
			rows=asc_actuators )


#def asc_intrix_pityaw(cols, rows, )
