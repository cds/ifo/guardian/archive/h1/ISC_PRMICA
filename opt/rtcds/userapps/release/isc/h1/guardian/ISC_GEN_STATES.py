# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: ISC_GEN_STATES.py 15017 2017-02-07 08:50:04Z thomas.shaffer@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/isc/h1/guardian/ISC_GEN_STATES.py $

#import multiprocess as mpc SHeila comme ted this out since it caused an error
import subprocess

from guardian import GuardState, GuardStateDecorator
from ISC_library import *
import ISC_library

##################################################
# GLOBALS
##################################################

def etmyBounceM0():
    return ezca.get_LIGOFilter('SUS-ETMY_M0_DARM_DAMP_V')
def itmxBounceM0():
    return ezca.get_LIGOFilter('SUS-ITMX_M0_DARM_DAMP_V')
def etmxBounceM0():
    return ezca.get_LIGOFilter('SUS-ETMX_M0_DARM_DAMP_V')
def itmyBounceM0():
    return ezca.get_LIGOFilter('SUS-ITMY_M0_DARM_DAMP_V')
def filt(channel):
    return ezca.get_LIGOFilter(channel)

##################################################
# Take care of WFS centering
##################################################
def gen_ENGAGE_REFL_WFS_CENTERING(dof):
    class ENGAGE_REFL_WFS_CENTERING(GuardState):
            request = False
            @assert_mc_locked
            @assert_dof_locked_gen(dof)
            #@get_subordinate_watchdog_check_decorator(self.nodes)
            #@nodes.checker()
            def main(self):
                if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                    notify('Toast is ready!')
                    #reset saturation counter                    
                    ezca['ISI-HAM6_SATCLEAR'] = 1
                    ezca['ISI-HAM6_WD_RSET'] = 1
                    time.sleep(1)
                    #ezca['ISI-HAM6_DACKILL_RESET'] = 1
                    ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter  
                # input matrix
                log('turning on DC centering')
                #ezca['ASC-INMATRIX_P_14_19'] = 1  # AS_A --> DC3 --> OM2
                #ezca['ASC-INMATRIX_Y_14_19'] = 1
                ezca.get_LIGOFilter('ASC-DC1_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('INPUT')

                '''# Turn on the OMC ASC gain gently, the compensation filters for the OMC SUS have some transient response
                cdsutils.step(ezca, 'OMC-ASC_MASTERGAIN','0.1,2',0.2)
                #temporarily commented out , SED March 30th because we were having truoble with AS_B being miscentered.
                # Don't use the boosts (FM1), it's too much gain
                # Integrators (FM2) should always be on
                #set_asc_boosts('ON')
                omc_asc_integrators('ON')'''
                self.timer['wait'] = 1

                if not ISC_library.REFL_WFS_DC_centering_servos_OK():  
                    notify('REFL or AS WFS DC centering')
                    ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')

                    ezca['ASC-DC1_P_RSET'] = 2
                    ezca['ASC-DC1_Y_RSET'] = 2
                    ezca['ASC-DC2_P_RSET'] = 2
                    ezca['ASC-DC2_Y_RSET'] = 2

                    self.timer['wait'] = 1
                    ezca.get_LIGOFilter('ASC-DC1_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('INPUT')

            def run(self):
                return True
                    
    return ENGAGE_REFL_WFS_CENTERING

def gen_REFL_WFS_CENTERING(dof):
    class REFL_WFS_CENTERING(GuardState):
        request = False
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(self.nodes)
        #@nodes.checker()
        def main(self):
            self.timer['step'] = 1

        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(self.nodes)
        #@nodes.checker()
        def run(self):
            if not ISC_library.REFL_WFS_DC_centering_servos_OK():
                notify('REFL or AS WFS DC centering railed')
                ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
                ezca['ASC-DC1_P_RSET'] = 2
                ezca['ASC-DC1_Y_RSET'] = 2
                ezca['ASC-DC2_P_RSET'] = 2
                ezca['ASC-DC2_Y_RSET'] = 2
                time.sleep(1)
                ezca.get_LIGOFilter('ASC-DC1_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('INPUT')
            # define thresholds
            threshold_dc = 0.2 # threshold on the WFS centering
            # wait for centering to reach a reasonable value
            err = ezcaAverageMultiple(('ASC-DC1_P_INMON', 'ASC-DC1_Y_INMON', 'ASC-DC2_P_INMON', 'ASC-DC2_Y_INMON'))  # removed DC4
            if max(abs(err)) > threshold_dc:
                if self.timer['step']:
                    if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                        notify('Toast is ready!')
                        ezca['ISI-HAM6_WD_RSET'] = 1
                        time.sleep(1)
                        ezca['ISI-HAM6_DACKILL_RESET'] = 1
                        ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter
                    err = ISC_library.ezcaAverageMultiple(('ASC-DC1_P_INMON', 'ASC-DC1_Y_INMON', 'ASC-DC2_P_INMON', 'ASC-DC2_Y_INMON'))  # removed DC4
                    self.timer['step'] = 1
            else:
                return True
    return REFL_WFS_CENTERING        

def gen_ENGAGE_AS_WFS_CENTERING(dof):
    class ENGAGE_AS_WFS_CENTERING(GuardState):
            request = False
            @assert_mc_locked
            @assert_dof_locked_gen(dof)
            #@get_subordinate_watchdog_check_decorator(self.nodes)
            #@nodes.checker()
            def main(self):
                if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                    notify('Toast is ready!')
                    #reset saturation counter                    
                    ezca['ISI-HAM6_SATCLEAR'] = 1
                    ezca['ISI-HAM6_WD_RSET'] = 1
                    time.sleep(1)
                    #ezca['ISI-HAM6_DACKILL_RESET'] = 1
                    ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter  
                # input matrix
                log('turning on DC centering')
                #ezca['ASC-INMATRIX_P_14_19'] = 1  # AS_A --> DC3 --> OM2
                #ezca['ASC-INMATRIX_Y_14_19'] = 1
                ezca.get_LIGOFilter('ASC-DC3_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_Y').switch_on('INPUT')

                '''# Turn on the OMC ASC gain gently, the compensation filters for the OMC SUS have some transient response
                cdsutils.step(ezca, 'OMC-ASC_MASTERGAIN','0.1,2',0.2)
                #temporarily commented out , SED March 30th because we were having truoble with AS_B being miscentered.
                # Don't use the boosts (FM1), it's too much gain
                # Integrators (FM2) should always be on
                #set_asc_boosts('ON')
                omc_asc_integrators('ON')'''
                self.timer['wait'] = 1

                if not ISC_library.AS_WFS_DC_centering_servos_OK():  
                    notify('REFL or AS WFS DC centering')
                    ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')

                    ezca['ASC-DC3_P_RSET'] = 2
                    ezca['ASC-DC3_Y_RSET'] = 2
                    ezca['ASC-DC4_P_RSET'] = 2
                    ezca['ASC-DC4_Y_RSET'] = 2

                    self.timer['wait'] = 1
                    ezca.get_LIGOFilter('ASC-DC3_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_Y').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_Y').switch_on('INPUT')

            def run(self):
                return True
                    
    return ENGAGE_AS_WFS_CENTERING

def gen_AS_WFS_CENTERING(dof):
    class AS_WFS_CENTERING(GuardState):
        request = False
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(self.nodes)
        #@nodes.checker()
        def main(self):
            self.timer['step'] = 1

        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(self.nodes)
        #@nodes.checker()
        def run(self):
            if not ISC_library.AS_WFS_DC_centering_servos_OK():
                notify('REFL or AS WFS DC centering railed')
                ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')
                ezca['ASC-DC3_P_RSET'] = 2
                ezca['ASC-DC3_Y_RSET'] = 2
                ezca['ASC-DC4_P_RSET'] = 2
                ezca['ASC-DC4_Y_RSET'] = 2
                time.sleep(1)
                ezca.get_LIGOFilter('ASC-DC3_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_Y').switch_on('INPUT')
            # define thresholds
            threshold_dc = 0.2 # threshold on the WFS centering
            # wait for centering to reach a reasonable value
            err = ezcaAverageMultiple(('ASC-DC3_P_INMON', 'ASC-DC3_Y_INMON', 'ASC-DC4_P_INMON', 'ASC-DC4_Y_INMON'))  
            if max(abs(err)) > threshold_dc:
                if self.timer['step']:
                    if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                        notify('Toast is ready!')
                        ezca['ISI-HAM6_WD_RSET'] = 1
                        time.sleep(1)
                        ezca['ISI-HAM6_DACKILL_RESET'] = 1
                        ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter
                    err = ISC_library.ezcaAverageMultiple(('ASC-DC3_P_INMON', 'ASC-DC3_Y_INMON', 'ASC-DC4_P_INMON', 'ASC-DC4_Y_INMON'))  # removed DC4
                    self.timer['step'] = 1
            else:
                return True
    return AS_WFS_CENTERING        


def gen_ENGAGE_CORNER_WFS_CENTERING(dof):
    class ENGAGE_CORNER_WFS_CENTERING(GuardState):
            request = False
            @assert_mc_locked
            @assert_dof_locked_gen(dof)
            #@get_subordinate_watchdog_check_decorator(self.nodes)
            #@nodes.checker()
            def main(self):
                if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                    notify('Toast is ready!')
                    #reset saturation counter                    
                    ezca['ISI-HAM6_SATCLEAR'] = 1
                    ezca['ISI-HAM6_WD_RSET'] = 1
                    time.sleep(1)
                    #ezca['ISI-HAM6_DACKILL_RESET'] = 1
                    ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter  
                # input matrix
                log('turning on DC centering')
                #ezca['ASC-INMATRIX_P_14_19'] = 1  # AS_A --> DC3 --> OM2
                #ezca['ASC-INMATRIX_Y_14_19'] = 1
                ezca.get_LIGOFilter('ASC-DC1_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_Y').switch_on('INPUT')

                '''# Turn on the OMC ASC gain gently, the compensation filters for the OMC SUS have some transient response
                cdsutils.step(ezca, 'OMC-ASC_MASTERGAIN','0.1,2',0.2)
                #temporarily commented out , SED March 30th because we were having truoble with AS_B being miscentered.
                # Don't use the boosts (FM1), it's too much gain
                # Integrators (FM2) should always be on
                #set_asc_boosts('ON')
                omc_asc_integrators('ON')'''
                self.timer['wait'] = 1

                if not ISC_library.CORNER_WFS_DC_centering_servos_OK():  
                    notify('REFL or AS WFS DC centering')
                    ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')
                    ezca['ASC-DC1_P_RSET'] = 2
                    ezca['ASC-DC1_Y_RSET'] = 2
                    ezca['ASC-DC2_P_RSET'] = 2
                    ezca['ASC-DC2_Y_RSET'] = 2
                    ezca['ASC-DC3_P_RSET'] = 2
                    ezca['ASC-DC3_Y_RSET'] = 2
                    ezca['ASC-DC4_P_RSET'] = 2
                    ezca['ASC-DC4_Y_RSET'] = 2

                    self.timer['wait'] = 1
                    ezca.get_LIGOFilter('ASC-DC1_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_Y').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_Y').switch_on('INPUT')



            def run(self):
                #if self.timer['wait']:
                #    ezca.get_LIGOFilter('ASC-DC1_P').switch_on('FM1')
                #    ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('FM1')
                #    ezca.get_LIGOFilter('ASC-DC2_P').switch_on('FM1')
                #    ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('FM1')
                return True
                    
    return ENGAGE_CORNER_WFS_CENTERING


def gen_CORNER_WFS_CENTERING(dof):
    class CORNER_WFS_CENTERING(GuardState):
        request = False
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(self.nodes)
        #@nodes.checker()
        def main(self):
            self.timer['step'] = 1

        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(self.nodes)
        #@nodes.checker()
        def run(self):
            if not CORNER_WFS_DC_centering_servos_OK():
                notify('REFL or AS WFS DC centering railed')
                ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')
                ezca['ASC-DC1_P_RSET'] = 2
                ezca['ASC-DC1_Y_RSET'] = 2
                ezca['ASC-DC2_P_RSET'] = 2
                ezca['ASC-DC2_Y_RSET'] = 2
                ezca['ASC-DC3_P_RSET'] = 2
                ezca['ASC-DC3_Y_RSET'] = 2
                ezca['ASC-DC4_P_RSET'] = 2
                ezca['ASC-DC4_Y_RSET'] = 2
                time.sleep(1)
                ezca.get_LIGOFilter('ASC-DC1_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_Y').switch_on('INPUT')
            # define thresholds
            threshold_dc = 0.2 # threshold on the WFS centering
            # wait for centering to reach a reasonable value
            err = ezcaAverageMultiple(('ASC-DC1_P_INMON', 'ASC-DC1_Y_INMON', 'ASC-DC2_P_INMON', 'ASC-DC2_Y_INMON', 'ASC-DC3_P_INMON', 'ASC-DC3_Y_INMON'))  # removed DC4
            if max(abs(err)) > threshold_dc:
                if self.timer['step']:
                    if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                        notify('Toast is ready!')
                        ezca['ISI-HAM6_WD_RSET'] = 1
                        time.sleep(1)
                        ezca['ISI-HAM6_DACKILL_RESET'] = 1
                        ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter
                    err = ISC_library.ezcaAverageMultiple(('ASC-DC1_P_INMON', 'ASC-DC1_Y_INMON', 'ASC-DC2_P_INMON', 'ASC-DC2_Y_INMON', 'ASC-DC3_P_INMON', 'ASC-DC3_Y_INMON'))  # removed DC4
                    self.timer['step'] = 1
            else:
                return True
    return CORNER_WFS_CENTERING

def gen_OFFLOAD_ALIGNMENT(dof, ramptime):
    class OFFLOAD_ALIGNMENT(GuardState):
        request = False
        redirect = False
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def main(self):

            old_gain_p = [None]*len(self.optics)
            old_gain_y = [None]*len(self.optics)

            #offload
            for i in range(len(self.optics)):
                log('starting smooth offload')
                smooth_offload(self.optics[i], ramptime) # changed from offload SED and CCW March 13 2015

        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def run(self):
            return True
    return OFFLOAD_ALIGNMENT

def gen_OFFLOAD_ALIGNMENT_MANY(dof, ramptime):
    class OFFLOAD_ALIGNMENT(GuardState):
        request = False
        redirect = False
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def main(self):

            old_gain_p = [None]*len(self.optics)
            old_gain_y = [None]*len(self.optics)
            old_tramp_p = [None]*len(self.optics)
            old_tramp_7 = [None]*len(self.optics)
            #offload
            log('starting smooth offload')
            ISC_library.smooth_offload_fast(self.optics, ramptime)
            #smooth_offload_many(self.optics, ramptime)

        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def run(self):
            return True
    return OFFLOAD_ALIGNMENT


# CHECKPOINT ORCA
def gen_VIOLIN_MODE_DAMPING(nodes):
    class VIOLIN_MODE_DAMPING(GuardState):
        index = 453
        request = False

        @ISC_library.get_watchdog_IMC_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            # Turn up bounce mode damping

            # GOOD FILTER SETTINGS - as of May 26 2015
            # All DARM_DAMP_V filter banks should have a broad butterworth BP filter in FM3
            # ETMY uses +60deg, negative gain
            # ITMX uses -60deg, positive gain
            # ETMX uses +60deg, negative gain
            # ITMY uses +60deg, +30deg, positive gain

            etmyBounceM0().GAIN.put(0.1)
            itmxBounceM0().GAIN.put(0.1)
            etmxBounceM0().GAIN.put(0.1)
            itmyBounceM0().GAIN.put(0.1)  #these used to be 0.3

            #ezca['SUS-ETMX_L2_DAMP_MODE2_GAIN'] = -50 #NK August 20th
            #ezca['SUS-ETMX_L2_DAMP_MODE3_GAIN'] = 50 #NK August 20th
            #ezca['SUS-ETMX_L2_DAMP_MODE4_GAIN'] = -300
            #ezca['SUS-ETMX_L2_DAMP_MODE6_GAIN'] = 100
            #ezca['SUS-ETMX_L2_DAMP_MODE10_GAIN'] = 100 #NK August 16th


            # ITMX
            ix_mode1 = ezca.get_LIGOFilter('SUS-ITMX_L2_DAMP_MODE1') 
            ix_mode2 = ezca.get_LIGOFilter('SUS-ITMX_L2_DAMP_MODE2') 
            ix_mode3 = ezca.get_LIGOFilter('SUS-ITMX_L2_DAMP_MODE3') 
            ix_mode4 = ezca.get_LIGOFilter('SUS-ITMX_L2_DAMP_MODE4') 
            ix_mode5 = ezca.get_LIGOFilter('SUS-ITMX_L2_DAMP_MODE5')
            ix_mode6 = ezca.get_LIGOFilter('SUS-ITMX_L2_DAMP_MODE6')
            ix_mode7 = ezca.get_LIGOFilter('SUS-ITMX_L2_DAMP_MODE7')  
            ix_mode8 = ezca.get_LIGOFilter('SUS-ITMX_L2_DAMP_MODE8')
            ix_mode9 = ezca.get_LIGOFilter('SUS-ITMX_L2_DAMP_MODE9') #NK24Jan17
            ix_mode10 = ezca.get_LIGOFilter('SUS-ITMX_L2_DAMP_MODE10')

            ix_mode1.GAIN.put(0)
            ix_mode1.TRAMP.put(10)
            ix_mode1.only_on('INPUT', 'FM1', 'FM3', 'FM4', 'OUTPUT', 'DECIMATION')
            ix_mode2.GAIN.put(0)
            ix_mode2.TRAMP.put(10)
            ix_mode2.only_on('INPUT', 'FM1', 'FM4', 'OUTPUT', 'DECIMATION')
            ix_mode3.GAIN.put(0)
            ix_mode3.TRAMP.put(10)
            ix_mode3.only_on('INPUT', 'FM1', 'FM2', 'FM4', 'OUTPUT', 'DECIMATION')
            ix_mode4.GAIN.put(0)
            ix_mode4.TRAMP.put(10)
            ix_mode4.only_on('INPUT', 'FM1', 'FM4', 'OUTPUT', 'DECIMATION')
            ix_mode5.GAIN.put(0)
            ix_mode5.TRAMP.put(10)
            ix_mode5.only_on('INPUT', 'FM1', 'FM4', 'OUTPUT', 'DECIMATION')
            ix_mode6.GAIN.put(0)
            ix_mode6.TRAMP.put(10)
            ix_mode6.only_on('INPUT', 'FM1', 'FM4', 'OUTPUT', 'DECIMATION')
            ix_mode7.GAIN.put(0)
            ix_mode7.TRAMP.put(10)
            ix_mode7.only_on('INPUT', 'FM1', 'FM4', 'OUTPUT', 'DECIMATION')
            ix_mode8.GAIN.put(0)
            ix_mode8.TRAMP.put(10)
            ix_mode8.only_on('INPUT', 'FM1', 'FM3', 'FM4', 'OUTPUT', 'DECIMATION')
            ix_mode9.TRAMP.put(10)
            ix_mode9.only_on('INPUT', 'FM8', 'FM2', 'FM4', 'OUTPUT', 'DECIMATION') #NK24Jan17
            ix_mode10.TRAMP.put(10)
            ix_mode10.only_on('INPUT', 'FM5', 'FM2', 'FM4', 'OUTPUT', 'DECIMATION') #NK24Jan17

            ix_mode1.GAIN.put(50) # doublechecked by KI 13/Jun/2016 -- It stopped damping at Gain +100 so I brough it down to 50 (NK 13 Jun)
            ix_mode2.GAIN.put(100) 
             
            ix_mode4.GAIN.put(10)
            ix_mode5.GAIN.put(1)
            
            ix_mode6.GAIN.put(100) # doublchecked by KI 13/Jun/2016
            ix_mode7.GAIN.put(50)
            ix_mode8.GAIN.put(10)
            ix_mode9.GAIN.put(-10) #NK24Jan17
            ix_mode10.GAIN.put(-10) #NK24Jan17

            # ITMY
            iy_mode = []
            iy_mode.append('') # 0th field is left empty
            for jj in range(1, 10+1):
                iy_mode.append( ezca.get_LIGOFilter( 'SUS-ITMY_L2_DAMP_MODE%d'%jj ) )
            for kk in range(1, 10+1):
                iy_mode[kk].GAIN.put(0)
                iy_mode[kk].TRAMP.put(10)

            iy_mode[1].only_on('INPUT', 'FM1', 'FM2', 'FM4', 'OUTPUT', 'DECIMATION')
            iy_mode[2].only_on('INPUT', 'FM1', 'FM2', 'FM4', 'OUTPUT', 'DECIMATION')
            iy_mode[3].only_on('INPUT', 'FM1',        'FM4', 'OUTPUT', 'DECIMATION')
            iy_mode[4].only_on('INPUT', 'FM1', 'FM4', 'FM5', 'OUTPUT', 'DECIMATION')
            iy_mode[5].only_on('INPUT', 'FM1', 'FM3', 'FM4', 'OUTPUT', 'DECIMATION')
            iy_mode[6].only_on('INPUT', 'FM1', 'FM2', 'FM4', 'OUTPUT', 'DECIMATION')
            iy_mode[7].only_on('INPUT', 'FM1', 'FM3', 'FM4', 'OUTPUT', 'DECIMATION')
            iy_mode[8].only_on('INPUT', 'FM1', 'FM2', 'FM4', 'OUTPUT', 'DECIMATION')
            iy_mode[9].only_on('INPUT', 'FM8', 'FM2', 'FM4', 'OUTPUT', 'DECIMATION') #NK24Jan17
            iy_mode[10].only_on('INPUT', 'FM8', 'FM2', 'FM4', 'OUTPUT', 'DECIMATION')#NK24Jan17

            # needed FM3 on June 14 2016
            #iy_mode[8].only_on('INPUT', 'FM1', 'FM3', 'FM4', 'OUTPUT', 'DECIMATION')

            iy_mode[1].GAIN.put(20) 
            iy_mode[2].GAIN.put(20)
            iy_mode[3].GAIN.put(20)
            iy_mode[4].GAIN.put(-10)
            iy_mode[5].GAIN.put(10)
            iy_mode[6].GAIN.put(-20)
            iy_mode[7].GAIN.put(50)
            iy_mode[8].GAIN.put(-50)
            iy_mode[9].GAIN.put(10) #NK24Jan2017
            iy_mode[10].GAIN.put(-10) #NK24Jan17

            # ETMX
            ex_mode = []
            ex_mode.append('') # 0th field is left empty
            for jj in range(1, 10+1):
                ex_mode.append( ezca.get_LIGOFilter( 'SUS-ETMX_L2_DAMP_MODE%d'%jj ) )
            for kk in range(1, 10+1):
                ex_mode[kk].GAIN.put(0)
                ex_mode[kk].TRAMP.put(10)

            ex_mode[1].only_on('INPUT', 'FM1', 'FM3', 'FM4', 'OUTPUT', 'DECIMATION')
            ex_mode[3].only_on('INPUT', 'FM1',        'FM4', 'OUTPUT', 'DECIMATION')
            ex_mode[4].only_on('INPUT', 'FM1', 'FM3', 'FM4', 'OUTPUT', 'DECIMATION')
            ex_mode[5].only_on('INPUT', 'FM1',        'FM4', 'OUTPUT', 'DECIMATION')
            # Note: for now mode 6 is broadband (includes 509.194Hz), but mode 7 only damps 509.194Hz. Together they work. Good enough for now.
            ex_mode[6].only_on('INPUT', 'FM1', 'FM3', 'FM4', 'FM5','OUTPUT', 'DECIMATION')
            ex_mode[7].only_on('INPUT', 'FM1', 'FM3', 'FM4', 'OUTPUT', 'DECIMATION')
            ex_mode[8].only_on('INPUT', 'FM1', 'FM4', 'OUTPUT', 'DECIMATION')
            ex_mode[9].only_on('INPUT', 'FM2', 'FM4', 'FM9', 'OUTPUT', 'DECIMATION') #NK24Jan17
            ex_mode[10].only_on('INPUT', 'FM1', 'FM2', 'FM4', 'OUTPUT', 'DECIMATION') #NK24Jan17

            ex_mode[1].GAIN.put(50)
            ex_mode[3].GAIN.put(100)
            ex_mode[4].GAIN.put(-50)
            ex_mode[5].GAIN.put(30)
            ex_mode[6].GAIN.put(-100)
            ex_mode[7].GAIN.put(150)
            ex_mode[8].GAIN.put(50)
            ex_mode[9].GAIN.put(10) #NK24Jan17
            ex_mode[10].GAIN.put(30) #NK24Jan17


            # ETMY
            ey_mode = []
            ey_mode.append('') # 0th field is left empty
            for jj in range(1, 10+1):
                ey_mode.append( ezca.get_LIGOFilter( 'SUS-ETMY_L2_DAMP_MODE%d'%jj ) )
            for kk in range(1, 10+1):
                ey_mode[kk].GAIN.put(0)
                ey_mode[kk].TRAMP.put(10)

            ey_mode[1].only_on('INPUT', 'FM4',        'FM6',  'OUTPUT', 'DECIMATION') #NK24Jan17
            ey_mode[2].only_on('INPUT', 'FM1',        'FM4',  'OUTPUT', 'DECIMATION')
            ey_mode[3].only_on('INPUT', 'FM1',        'FM4',  'OUTPUT', 'DECIMATION')
            ey_mode[4].only_on('INPUT', 'FM1', 'FM3', 'FM4',  'OUTPUT', 'DECIMATION')
            ey_mode[5].only_on('INPUT', 'FM1', 'FM3', 'FM4',  'OUTPUT', 'DECIMATION')
            ey_mode[6].only_on('INPUT', 'FM1', 'FM3', 'FM4', 'FM5',  'OUTPUT', 'DECIMATION')
            ey_mode[7].only_on('INPUT', 'FM1', 'FM3', 'FM4',  'OUTPUT', 'DECIMATION')
            ey_mode[8].only_on('INPUT', 'FM1', 'FM3', 'FM4',  'OUTPUT', 'DECIMATION')
            ey_mode[10].only_on('INPUT', 'FM4', 'FM10',  'OUTPUT', 'DECIMATION') # 4.73509k no Phase Filter for better damping. LHO aLOG 33735
            
            ey_mode[1].GAIN.put(15) #NK24Jan17
            ey_mode[2].GAIN.put(-40)
            ey_mode[3].GAIN.put(-20)
            ey_mode[4].GAIN.put(-20)
            ey_mode[5].GAIN.put(-40)
            ey_mode[6].GAIN.put(+50)
            ey_mode[7].GAIN.put(-60)
            ey_mode[8].GAIN.put(-50)
            ey_mode[10].GAIN.put(0.02) # Moved 4.73509k from MODE9 to MODE10, improved Phase Filters. JK/JD 12/01 LHO aLOG 32081


            # the following violins were saturating on March 11 2016 with above gains - turn them down
            time.sleep(10)
            adjustDamperGain('SUS-ETMX_L2_DAMP_MODE4',-500,1.5e5)
            adjustDamperGain('SUS-ETMX_L2_DAMP_MODE6',-100,1.5e5)
            #adjustDamperGain('SUS-ETMY_L2_DAMP_MODE1',-100,1.5e5)
            adjustDamperGain('SUS-ETMY_L2_DAMP_MODE6',+500,1.5e5)
            adjustDamperGain('SUS-ETMY_L2_DAMP_MODE8',-500,1.5e5)
            adjustDamperGain('SUS-ITMX_L2_DAMP_MODE3',100,1.5e5)
            adjustDamperGain('SUS-ITMX_L2_DAMP_MODE5',100,1.5e5)

            # turn up dampinmg gain by 10 if possible
            #adjustDamperGain('SUS-ITMY_L2_DAMP_MODE1',200,1.5e5)
            adjustDamperGain('SUS-ITMY_L2_DAMP_MODE2',200,1.5e5)
            adjustDamperGain('SUS-ITMY_L2_DAMP_MODE3',200,1.5e5) 
            adjustDamperGain('SUS-ITMY_L2_DAMP_MODE4',-100,1.5e5)
            adjustDamperGain('SUS-ITMY_L2_DAMP_MODE5',100,1.5e5)
            #adjustDamperGain('SUS-ITMY_L2_DAMP_MODE6',-200,1.5e5)
            adjustDamperGain('SUS-ITMY_L2_DAMP_MODE7',500,1.5e5)
            adjustDamperGain('SUS-ITMY_L2_DAMP_MODE8',-50,1.5e5)

        @ISC_library.get_watchdog_IMC_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            return True
    return VIOLIN_MODE_DAMPING

'''def gen_OFFLOAD_ALIGNMENT_SIMULT(dof, ramptime):
    # =====> UNTESTED
    class OFFLOAD_ALIGNMENT(GuardState):
        request = True
        redirect = False
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def main(self):

            old_gain_p = [None]*len(self.optics)
            old_gain_y = [None]*len(self.optics)

            #offload
            for i in range(len(self.optics)):
                log('starting smooth offload')
                p = mpc.Process(target=smooth_offload, args=(self.optics[i], ramptime))
                p.start()

        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def run(self):
            return True
    return OFFLOAD_ALIGNMENT'''


