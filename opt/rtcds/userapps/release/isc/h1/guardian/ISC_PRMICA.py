# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id$
# $HeadURL$

import time
import cdsutils as cdu
from guardian import GuardState, GuardStateDecorator
from guardian.ligopath import userapps_path
from guardian import NodeManager
import ISC_library as i
from ISC_GEN_STATES import *
import lscparams as par
import fast_ezca as fez

# scripts that we shouldn't be using
#drmi_to_1f_script_file = userapps_path('lsc', IFO.lower(), 'scripts', 'autolock', 'drfpmi', 'drmi_to_1f.sh')
#aligner_script_file = userapps_path('sus', 'common', 'scripts', 'aligner.py')

nominal = 'IDLE'

##################################################
## name the filters
##################################################
def filt(channel):
    return ezca.get_LIGOFilter(channel)

def prcl_filt():
    return ezca.get_LIGOFilter('LSC-PRCL1')

def mich_filt():
    return ezca.get_LIGOFilter('LSC-MICH1')

def srcl_filt():
    return ezca.get_LIGOFilter('LSC-SRCL1')

def asc_dc1_yaw_filter():
    return ezca.get_LIGOFilter('ASC-DC1_Y')

def asc_dc1_pitch_filter():
    return ezca.get_LIGOFilter('ASC-DC1_P')

def asc_dc2_yaw_filter():
    return ezca.get_LIGOFilter('ASC-DC2_Y')

def asc_dc2_pitch_filter():
    return ezca.get_LIGOFilter('ASC-DC2_P')

def asc_dc3_yaw_filter():
    return ezca.get_LIGOFilter('ASC-DC3_Y')

def asc_dc3_pitch_filter():
    return ezca.get_LIGOFilter('ASC-DC3_P')

def asc_dc4_yaw_filter():
    return ezca.get_LIGOFilter('ASC-DC4_Y')

def asc_dc4_pitch_filter():
    return ezca.get_LIGOFilter('ASC-DC4_P')

def asc_prc1_yaw_filter():
    return ezca.get_LIGOFilter('ASC-PRC1_Y')

def asc_prc1_pitch_filter():
    return ezca.get_LIGOFilter('ASC-PRC1_P')

def asc_prc2_yaw_filter():
    return ezca.get_LIGOFilter('ASC-PRC2_Y')

def asc_prc2_pitch_filter():
    return ezca.get_LIGOFilter('ASC-PRC2_P')

def asc_mich_yaw_filter():
    return ezca.get_LIGOFilter('ASC-MICH_Y')

def asc_mich_pitch_filter():
    return ezca.get_LIGOFilter('ASC-MICH_P')

def asc_src1_yaw_filter():
    return ezca.get_LIGOFilter('ASC-SRC1_Y')

def asc_src1_pitch_filter():
    return ezca.get_LIGOFilter('ASC-SRC1_P')

def asc_src2_yaw_filter():
    return ezca.get_LIGOFilter('ASC-SRC2_Y')

def asc_src2_pitch_filter():
    return ezca.get_LIGOFilter('ASC-SRC2_P')

def asc_inp1_yaw_filter():
    return ezca.get_LIGOFilter('ASC-INP1_Y')

def asc_inp1_pitch_filter():
    return ezca.get_LIGOFilter('ASC-INP1_P')

def prm_m2_filt():
    return ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L')

def srm_m2_filt():
    return ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L')

# BS top stage LOCK_P and _Y
def sus_bs_m1_pit_filter():
    return ezca.get_LIGOFilter('SUS-BS_M1_LOCK_P')
def sus_bs_m1_yaw_filter():
    return ezca.get_LIGOFilter('SUS-BS_M1_LOCK_Y')
# PRM top stage LOCK_P and _Y
def sus_prm_m1_pit_filter():
    return ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_P')
def sus_prm_m1_yaw_filter():
    return ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_Y')
def sus_pr2_m1_pit_filter():
    return ezca.get_LIGOFilter('SUS-PR2_M1_LOCK_P')
def sus_pr2_m1_yaw_filter():
    return ezca.get_LIGOFilter('SUS-PR2_M1_LOCK_Y')
# PR3 top stage LOCK_P and _Y
def sus_pr3_m1_pit_filter():
    return ezca.get_LIGOFilter('SUS-PR3_M1_LOCK_P')
def sus_pr3_m1_yaw_filter():
    return ezca.get_LIGOFilter('SUS-PR3_M1_LOCK_Y')
# SRM top stage LOCK_P and _Y
def sus_srm_m1_pit_filter():
    return ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_P')
def sus_srm_m1_yaw_filter():
    return ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_Y')
# SR2 top stage LOCK_P and _Y
def sus_sr2_m1_pit_filter():
    return ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_P')
def sus_sr2_m1_yaw_filter():
    return ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_Y')
# SR3 top stage LOCK_P and _Y
def sus_sr3_m1_pit_filter():
    return ezca.get_LIGOFilter('SUS-SR3_M1_LOCK_P')
def sus_sr3_m1_yaw_filter():
    return ezca.get_LIGOFilter('SUS-SR3_M1_LOCK_Y')

##################################################
# STATES
##################################################
class INIT(GuardState):
     def main(self):
         log("initializing subordinate nodes...")
     def run(self):
         return True

class IDLE(GuardState):
    def run(self):
        return True

class LOCKLOSS(GuardState):
    """Record lockloss events"""
    request = False

    def main(self):
        return True

# reset everything to the good values for acquistion.  These values
# are stored in the down script.
class DOWN(GuardState):
    index = 10
    goto = True
    def main(self):
        #ramp off ASC loops (new stuff)
        for asc_loop in ['MICH', 'PRC1', 'PRC2', 'SRC1', 'SRC2','INP1']:
            for dof in ['P', 'Y']:                
                ezca['ASC-{}_{}_TRAMP'.format(asc_loop, dof)] = 1
                filt('ASC-{}_{}'.format(asc_loop, dof)).switch('INPUT', 'OFF', wait=False)
        for asc_loop in ['MICH', 'PRC1', 'PRC2', 'SRC1', 'SRC2','INP1']:
            for dof in ['P', 'Y']: 
                ezca['ASC-{}_{}_GAIN'.format(asc_loop, dof)] = 0

        # ramp off IM4 SRM, SR3, PRM, PR3 M1 LOCK (integrators)
        for sus in ['IM4', 'SRM', 'SR2', 'PRM', 'PR2', 'BS', 'PR3']:
            for dof in ['P', 'Y']: 
                ezca['SUS-{}_M1_LOCK_{}_TRAMP'.format(sus,dof)]=1
                filt('SUS-{}_M1_LOCK_{}'.format(sus,dof)).switch('INPUT', 'OFF', wait=False)
        #treat some sus differently because of wire heating
        sus_bs_m1_pit_filter().switch_off('FM1')   # BS PIT gets treated separately because of wire heating
        sus_pr3_m1_pit_filter().switch_off('FM1')  # PR3 PIT gets treated separately because of wire heating
  
        for sus in ['IM4', 'SRM', 'SR2', 'PRM', 'PR2']:
            for dof in ['P', 'Y']: 
                ezca['SUS-{}_M1_LOCK_{}_GAIN'.format(sus,dof)]=0
        #treat some sus differently because of wire heating
        for sus in ['BS', 'PR3']:
            ezca['SUS-{}_M1_LOCK_Y_GAIN'.format(sus,dof)]=0
        #ramp off LSC loops and top mass LSC drives
        for lsc_loop in ['MICH', 'PRCL', 'SRCL']:
            filt('LSC-{}1'.format(lsc_loop)).switch('INPUT', 'OFF', wait=False)
            ezca['LSC-{}1_TRAMP'.format(lsc_loop)]=1
        for lsc_loop in ['MICH', 'PRCL', 'SRCL']:
            ezca['LSC-{}1_GAIN'.format(lsc_loop)]=0
        for lsc_sus in ['PRM', 'BS', 'SRM']:
                ezca['SUS-{}_M1_LOCK_L_TRAMP'.format(sus)]=1
                filt('SUS-{}_M1_LOCK_L'.format(sus)).switch('INPUT', 'OFF', wait=False)
        for lsc_sus in ['PRM', 'BS', 'SRM']:
                ezca['SUS-{}_M1_LOCK_L_GAIN'.format(sus)]=0
        
        # pause to let all the gains ramp to 0    
        time.sleep(1)
        # clear history of ASC loops and sus top masses
        for asc_loop in ['MICH', 'PRC1', 'PRC2', 'SRC1', 'SRC2','INP1']:
            for dof in ['P', 'Y']: 
                ezca['ASC-{}_{}_RSET'.format(asc_loop, dof)] = 2
        time.sleep(10)
        for sus in ['IM4', 'SRM', 'SR2', 'PRM', 'PR2']:
            for dof in ['P', 'Y']: 
                ezca['SUS-{}_M1_LOCK_{}_RSET'.format(sus,dof)]=2
        for sus in ['BS', 'PR3']:
            ezca['SUS-{}_M1_LOCK_Y_RSET'.format(sus,dof)]=2
        #treat some sus differently because of wire heating
        sus_pr3_m1_pit_filter().switch_on('FM1')
        sus_bs_m1_pit_filter().switch_on('FM1')
        #reset top mass gains
        for sus in ['IM4', 'SRM', 'SR2', 'PRM', 'PR2']:
            for dof in ['P', 'Y']: 
                ezca['SUS-{}_M1_LOCK_{}_GAIN'.format(sus,dof)]=1
        ezca['SUS-BS_M1_LOCK_P_GAIN'] = -1
        ezca['SUS-BS_M1_LOCK_Y_GAIN'] = -1
        #ezca['SUS-PR3_M1_LOCK_P_GAIN'] = 1  # PR3 PIT gets treated separately because of wire heating
        ezca['SUS-PR3_M1_LOCK_Y_GAIN'] = 1
        
        # BS length M1 feed-back setup
        ezca['SUS-BS_M1_LOCK_L_GAIN'] = 0.1
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_L').switch_off('INPUT','FM1')

        # BS and PR3 OPLEV back on
        ezca['SUS-BS_M2_OLDAMP_P_GAIN'] = 300
        ezca['SUS-BS_M2_OLDAMP_Y_GAIN'] = 650
        
        ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L').switch_off('INPUT')
        ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L').switch_off('INPUT')
        ezca['SUS-SRM_M2_LOCK_L_GAIN'] = 0
        ezca['SUS-PRM_M2_LOCK_L_GAIN'] = 0
        ezca['SUS-PRM_M2_LOCK_L_RSET'] = 2
        ezca['SUS-SRM_M2_LOCK_L_RSET'] = 2

        # turn off DC centering
        ezca['OMC-ASC_MASTERGAIN'] = 0
        ezca['OMC-ASC_QDSLIDER'] = 1
        # clear history of the OMC ASC loops
        chans = []
        for dof in ['X','Y']:
            for basis in ['ANG','POS']:
                chans.append('OMC-ASC_' + basis + '_' + dof)
        for j in range(len(chans)):
            ezca[chans[j] + '_RSET'] = 2

        asc_dc1_yaw_filter().switch_off('INPUT')    
        asc_dc1_pitch_filter().switch_off('INPUT')
        asc_dc2_yaw_filter().switch_off('INPUT')
        asc_dc2_pitch_filter().switch_off('INPUT')
        asc_dc3_yaw_filter().switch_off('INPUT')    
        asc_dc3_pitch_filter().switch_off('INPUT')
        asc_dc4_yaw_filter().switch_off('INPUT')
        asc_dc4_pitch_filter().switch_off('INPUT')
        ezca['ASC-DC1_P_RSET'] = 2
        ezca['ASC-DC1_Y_RSET'] = 2
        ezca['ASC-DC2_P_RSET'] = 2
        ezca['ASC-DC2_Y_RSET'] = 2
        ezca['ASC-DC3_P_RSET'] = 2
        ezca['ASC-DC3_Y_RSET'] = 2
        ezca['ASC-DC4_P_RSET'] = 2
        ezca['ASC-DC4_Y_RSET'] = 2

        # set lower SUS stages to high range
        for section in ['UR','UL','LR','LL']:
            #SED EDH commented out switching for PR2 SR2 Sept 8 2015
            ezca['SUS-PRM_BIO_M3_{}_STATEREQ'.format(section)] = 2
            ezca['SUS-SRM_BIO_M3_{}_STATEREQ'.format(section)] = 2
        ezca['SUS-PRM_BIO_M2_STATEREQ'] = 3
        # put back in regular matrix
        coilburtpath = '/opt/rtcds/userapps/release/isc/h1/scripts/sus/'
        stage='M3'
        for optic in ['PRM','PR2','SRM','SR2']:
            ezca.burtwb(coilburtpath+ optic.lower() + '_' + stage.lower() +'_out_normal.snap')
        optic='BS'
        stage='M2'
        ezca.burtwb(coilburtpath+ optic.lower() + '_' + stage.lower() +'_out_normal.snap')
        time.sleep(1) # for burt to work
        stage='M3'
        for optic in ['PRM','PR2','SRM','SR2']:
            ezca['SUS-' + optic + '_' + stage + '_EUL2OSEM_LOAD_MATRIX'] = 1
        optic='BS'
        stage='M2'
        ezca['SUS-' + optic + '_' + stage + '_EUL2OSEM_LOAD_MATRIX'] = 1

        # set BS middle stage back to high range
        for coil in ('UL', 'UR', 'LL', 'LR'):
            ezca['SUS-BS_BIO_M2_%s_STATEREQ'%coil] = 2

        #undo gain increase for SRM, PRM offloading SED SEpt 7 2015
        ezca['SUS-PRM_M2_LOCK_L_GAIN'] = 1
        ezca['SUS-SRM_M2_LOCK_L_GAIN'] = 1
        # end SED SEpt 7 edits
        # set trigger thresholds back
        ezca['LSC-MICH_TRIG_THRESH_OFF'] = par.prmicar_mich_trig_lower_threshold
        ezca['LSC-MICH_FM_TRIG_THRESH_OFF'] = par.prmicar_mich_trig_fm_lower_threshold
        ezca['LSC-PRCL_FM_TRIG_THRESH_OFF'] = par.prmicar_prcl_trig_fm_lower_threshold

        #turn off SRM dither
        ezca['ASC-ADS_YAW1_OSC_CLKGAIN']=0
        ezca['ASC-ADS_PIT1_OSC_CLKGAIN']=0
        ezca['ASC-ADS_YAW1_DOF_GAIN']=0
        ezca['ASC-ADS_PIT1_DOF_GAIN']=0
        ezca['ASC-ADS_YAW2_DOF_GAIN']=0
        ezca['ASC-ADS_PIT2_DOF_GAIN']=0

        ezca['ASC-AS90_OVER_POP90_GAIN']=0

    def run(self):
        if MC_locked():
            return True
        else:
            notify('MC is not locked')


# P(D)RMI Locking

# Turn on the feedback for MICH, PRCL, SRCL, turn on the feedback for
# M2 stage integrators in PRM/SRM when POP18I goes above threshold 
# for 200 msec
class SET_PRMI(GuardState):
    index = 20
    request = False
    @assert_mc_locked
    #@nodes.checker()
    def main(self):
        # set prcl, mich, srcl filters and gain
        mich_filt().only_on('DECIMATION', 'OUTPUT')
        for jj in range(0, len(par.mich_acquire_FMs)):
            mich_filt().switch_on(par.mich_acquire_FMs[jj])
        ezca.get_LIGOFilter('LSC-MICH2').only_on('INPUT','FM1', 'FM2', 'OUTPUT', 'DECIMATION')
        prcl_filt().only_on('DECIMATION', 'OUTPUT')
        for jj in range(0, len(par.prcl_acquire_FMs)):
            prcl_filt().switch_on(par.prcl_acquire_FMs[jj])

        for DOFname in ('MICH', 'PRCL', 'SRCL'):
            i.intrix.zero(row=DOFname)
        # set LSC ouput matrix
        i.outrix['PRM', 'PRCL'] = 1
        i.outrix['BS', 'MICH'] = 1

        # set trigger matrix
        i.trigrix['SRCL', 'ASAIR_A_DC'] = 0
        for DOFname in ('MICH', 'PRCL'):    
            i.trigrix[DOFname, 'POPAIR_B_RF18_I'] = 1
        i.trigrix['MICH','ASAIR_B_RF90_I'] = 0
        i.trigrix['REFLBIAS', 'TRX'] = 0
        i.trigrix['REFLBIAS', 'TRY'] = 0

        ezca['LSC-REFLBIAS_TRIG_THRESH_ON'] = -100
        ezca['LSC-REFLBIAS_TRIG_THRESH_OFF'] = -100

        # set FM trigers through FM1 to FM10
        # They use the same set of filter masking as DRMI
        for jj in range(1, 11):
            if jj in par.drmi_mich_fm_trig_index:
                ezca['LSC-MICH_MASK_FM%d'%jj] = 1
            else:
                ezca['LSC-MICH_MASK_FM%d'%jj] = 0

            if jj in par.drmi_prcl_fm_trig_index:
                ezca['LSC-PRCL_MASK_FM%d'%jj] = 1
            else:
                ezca['LSC-PRCL_MASK_FM%d'%jj] = 0

        log('setting MICH trigger thresholds')
        ezca['LSC-MICH_TRIG_THRESH_ON'] = par.prmicar_mich_trig_upper_threshold
        ezca['LSC-MICH_TRIG_THRESH_OFF'] = par.prmicar_mich_trig_lower_threshold
        ezca['LSC-MICH_FM_TRIG_THRESH_ON'] = par.prmicar_mich_trig_fm_upper_threshold
        ezca['LSC-MICH_FM_TRIG_THRESH_OFF'] = par.prmicar_mich_trig_fm_lower_threshold
            
        ezca['LSC-PRCL_TRIG_THRESH_ON'] = par.prmicar_prcl_trig_upper_threshold
        ezca['LSC-PRCL_TRIG_THRESH_OFF'] = par.prmicar_prcl_trig_lower_threshold
        # set fm trigger thresholds
        ezca['LSC-PRCL_FM_TRIG_THRESH_ON'] = par.prmicar_prcl_trig_fm_upper_threshold
        ezca['LSC-PRCL_FM_TRIG_THRESH_OFF'] = par.prmicar_prcl_trig_fm_lower_threshold
     
class LOCK_PRMI_CARRIER(GuardState):
    index = 30
    request = False
    @assert_mc_locked
    #@nodes.checker()
    def main(self): 
        # BS limiter ON
        ezca['SUS-BS_M3_ISCINF_L_LIMIT'] = 500000
        filt('SUS-BS_M3_ISCINF_L').switch('LIMIT', 'ON', wait=True)
        filt('SUS-BS_M3_ISCINF_L').switch('FM6', 'OFF', wait=False) # Jan-5-2016

        self.lock_timer_on = False;

        ready_for_locking()
        # turn off asc and top stage
        fez.switch_many(ezca, [ \
          (asc_dc4_yaw_filter(), 'INPUT', 'OFF'), (asc_dc4_pitch_filter(), 'INPUT', 'OFF'), \
          (asc_dc3_yaw_filter(), 'INPUT', 'OFF'), (asc_dc3_pitch_filter(), 'INPUT', 'OFF'), \
          (asc_prc1_pitch_filter(), 'INPUT', 'OFF'), (asc_prc1_yaw_filter(), 'INPUT', 'OFF'), \
          (asc_prc2_pitch_filter(), 'INPUT', 'OFF'), (asc_prc2_yaw_filter(), 'INPUT', 'OFF'), \
          (asc_mich_pitch_filter(), 'INPUT', 'OFF'), (asc_mich_yaw_filter(), 'INPUT', 'OFF'), \
          (asc_src1_pitch_filter(), 'INPUT', 'OFF'), (asc_src1_yaw_filter(), 'INPUT', 'OFF'), \
          (asc_src2_pitch_filter(), 'INPUT', 'OFF'), (asc_src2_yaw_filter(), 'INPUT', 'OFF'), \
          (asc_inp1_pitch_filter(), 'INPUT', 'OFF'), (asc_inp1_yaw_filter(), 'INPUT', 'OFF'), \
          (sus_bs_m1_pit_filter(), 'INPUT', 'OFF'), (sus_bs_m1_yaw_filter(), 'INPUT', 'OFF'), \
          (sus_prm_m1_pit_filter(), 'INPUT', 'OFF'), (sus_prm_m1_yaw_filter(), 'INPUT', 'OFF'), \
          (sus_pr2_m1_pit_filter(), 'INPUT', 'OFF'), (sus_pr2_m1_yaw_filter(), 'INPUT', 'OFF'), \
          (sus_pr3_m1_pit_filter(), 'INPUT', 'OFF'), (sus_pr3_m1_yaw_filter(), 'INPUT', 'OFF'), \
          (sus_sr2_m1_pit_filter(), 'INPUT', 'OFF'), (sus_sr2_m1_yaw_filter(), 'INPUT', 'OFF'), \
          (sus_srm_m1_pit_filter(), 'INPUT', 'OFF'), (sus_srm_m1_yaw_filter(), 'INPUT', 'OFF'), \
          (sus_sr3_m1_pit_filter(), 'INPUT', 'OFF'), (sus_sr3_m1_yaw_filter(), 'INPUT', 'OFF')])

        # clear history of ASC loops
        # and set gain to zero
        fez.write_many(ezca, [ \
          ('ASC-INP1_P_RSET', 2), ('ASC-INP1_Y_RSET', 2), \
          ('ASC-PRC1_P_RSET', 2), ('ASC-PRC1_Y_RSET', 2), \
          ('ASC-PRC2_P_RSET', 2), ('ASC-PRC2_Y_RSET', 2), \
          ('ASC-SRC1_P_RSET', 2), ('ASC-SRC1_Y_RSET', 2), \
          ('ASC-SRC2_P_RSET', 2), ('ASC-SRC2_Y_RSET', 2), \
          ('ASC-MICH_P_RSET', 2), ('ASC-MICH_Y_RSET', 2), \
          ('SUS-IM4_M1_LOCK_P_GAIN', 0), ('SUS-IM4_M1_LOCK_Y_GAIN', 0), \
          ('SUS-SRM_M1_LOCK_P_GAIN', 0), ('SUS-SRM_M1_LOCK_Y_GAIN', 0), \
          ('SUS-SR2_M1_LOCK_P_GAIN', 0), ('SUS-SR2_M1_LOCK_Y_GAIN', 0), \
          ('SUS-PRM_M1_LOCK_P_GAIN', 0), ('SUS-PRM_M1_LOCK_Y_GAIN', 0), \
          ('SUS-PR2_M1_LOCK_P_GAIN', 0), ('SUS-PR2_M1_LOCK_Y_GAIN', 0), \
          ('SUS-BS_M1_LOCK_P_GAIN', 0), ('SUS-BS_M1_LOCK_Y_GAIN', 0)])
        time.sleep(5)

        #  clear history of IM4 SRM, SR3, PRM, PR3 M1 LOCK (integrators)
        #  and reset gains
        fez.write_many(ezca, [ \
          ('SUS-IM4_M1_LOCK_P_RSET', 2), ('SUS-IM4_M1_LOCK_Y_RSET', 2), \
          ('SUS-SRM_M1_LOCK_P_RSET', 2), ('SUS-SRM_M1_LOCK_Y_RSET', 2), \
          ('SUS-SR2_M1_LOCK_P_RSET', 2), ('SUS-SR2_M1_LOCK_Y_RSET', 2), \
          ('SUS-PRM_M1_LOCK_P_RSET', 2), ('SUS-PRM_M1_LOCK_Y_RSET', 2), \
          ('SUS-PR2_M1_LOCK_P_RSET', 2), ('SUS-PR2_M1_LOCK_Y_RSET', 2), \
          ('SUS-BS_M1_LOCK_P_RSET',  2), ('SUS-BS_M1_LOCK_Y_RSET',  2), \
          ('SUS-IM4_M1_LOCK_P_GAIN', 1), ('SUS-IM4_M1_LOCK_Y_GAIN', 1), \
          ('SUS-SRM_M1_LOCK_P_GAIN', 1), ('SUS-SRM_M1_LOCK_Y_GAIN', 1), \
          ('SUS-SR2_M1_LOCK_P_GAIN', 1), ('SUS-SR2_M1_LOCK_Y_GAIN', 1), \
          ('SUS-PRM_M1_LOCK_P_GAIN', 1), ('SUS-PRM_M1_LOCK_Y_GAIN', 1), \
          ('SUS-PR2_M1_LOCK_P_GAIN', 1), ('SUS-PR2_M1_LOCK_Y_GAIN', 1), \
          ('SUS-BS_M1_LOCK_P_GAIN', -1), ('SUS-BS_M1_LOCK_Y_GAIN', -1)])

        if not i.CORNER_WFS_DC_centering_servos_OK():
            log('Reset DC centering')
            fez.write_many(ezca, [ \
              ('ASC-DC1_P_RSET', 2), ('ASC-DC1_Y_RSET', 2), \
              ('ASC-DC2_P_RSET', 2), ('ASC-DC2_Y_RSET', 2), \
              ('ASC-DC3_P_RSET', 2), ('ASC-DC3_Y_RSET', 2), \
              ('ASC-DC4_P_RSET', 2), ('ASC-DC3_Y_RSET', 2)])


        # make sure to use OLDAMP on BS
        ezca['SUS-BS_M2_OLDAMP_P_GAIN'] = 300
        ezca['SUS-BS_M2_OLDAMP_Y_GAIN'] = 650
        ezca['ASC-MICH_P_GAIN'] = 0 # 1
        ezca['ASC-MICH_Y_GAIN'] = 0 # 1
        ezca.get_LIGOFilter('ASC-MICH_P').only_on('INPUT', 'FM1','FM3','FM7','FM10','OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-MICH_Y').only_on('INPUT','FM1','FM3','FM4','FM8','FM10','OUTPUT', 'DECIMATION')
        i.intrix.zero(row='MICH')
        i.intrix.zero(row='PRCL')
        i.intrix.zero(row='SRCL')

        i.intrix['MICH', 'REFLAIR_A45Q'] = par.mich_input_mtrx
        i.intrix['PRCL', 'REFLAIR_A9I']= par.prcl_input_mtrx
        i.intrix['SRCL', 'REFLAIR_A9I'] = - 3.75

        #turn off SRM dither
        ezca['ASC-ADS_YAW1_OSC_CLKGAIN']=0
        ezca['ASC-ADS_PIT1_OSC_CLKGAIN']=0
        ezca['ASC-ADS_YAW1_DOF_GAIN']=0
        ezca['ASC-ADS_PIT1_DOF_GAIN']=0
        
        i.intrix.load()
        # Check that the ramp matrix loaded correctly
        chansList = [(3, 16), (4, 13), (5, 13), (5, 15)]
        matrixGood = loadRampMatrix('LSC-PD_DOF_MTRX', chansList, 3)
        # Set LSC ouput matrix
        mich_filt().switch_off('FM2')
        # Turn off feedback to M2 stages of PRM and SRM.
        # Then prepare the filters for offloading
        #prm_m2_filt().ramp_gain(0, 2, wait=False)
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').ramp_gain(0, 2, wait=False)
        ezca['SUS-PRM_M2_LOCK_OUTSW_L'] = 0
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').only_on('FM2', 'FM4', 'FM6', 'FM8','FM9',
                             'FM10', 'INPUT', 'OUTPUT', 'DECIMATION', wait=False)
        ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L').only_on('INPUT', 'OUTPUT', 'DECIMATION', wait=False)
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').ramp_gain(0, 2, wait=False)
        ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L').ramp_gain(1, 2, wait=False)
        #set up offloading for SRM
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').ramp_gain(0, 2, wait=False)
        ezca['SUS-SRM_M2_LOCK_OUTSW_L'] = 0
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').only_on('FM2', 'FM4', 'FM6', 'FM8',
                             'FM10', 'INPUT', 'OUTPUT', 'DECIMATION', wait=False)
        ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L').only_on('INPUT', 'OUTPUT', 'DECIMATION', wait=False)
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').ramp_gain(0, 2, wait=False)
        ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L').ramp_gain(1, 2, wait=False)

        # Turn off feedback to M1 stage of BS
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_L').switch_off('INPUT','FM1')

        # Turn on DRMI loops
        mich_filt().switch('INPUT','OUTPUT', 'ON', wait=False)
        prcl_filt().switch('INPUT','OUTPUT', 'ON', wait=False)
        srcl_filt().switch('INPUT','OUTPUT', 'ON', wait=False)

        log('setting gains for DRMI with arms')
        mich_filt().ramp_gain(par.prmicar_mich_gain, 2, wait=False)
        prcl_filt().ramp_gain(par.prmicar_prcl_gain, 2, wait=False)
        PSLinputpower_OK()

        # set trigger thresholds
        ezca['LSC-MICH2_GAIN']=1
        ezca['LSC-PRCL_MASK_FM3']=0#
        ezca['LSC-MICH_MASK_FM3']=0#
        # Vertex LSC filters
        ezca.switch('SUS-BS_M3_ISCINF_L', 'FM4','FM5', 'OFF')
        ezca.switch('SUS-BS_M2_LOCK_L', 'FM4', 'FM5', 'FM9', 'FM10', 'ON')
        ezca.switch('SUS-BS_M3_LOCK_L', 'FM2', 'ON')
        if i.DRMI_locked():
            return True
        ezca.get_LIGOFilter('LSC-MICH1').only_on('INPUT', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('LSC-MICH2').only_on('INPUT','FM1', 'FM2', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('LSC-PRCL1').only_on('INPUT', 'FM4', 'FM9', 'FM10', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('LSC-PRCL2').only_on('INPUT', 'OUTPUT', 'DECIMATION')
        #get rid of annoying flashes on strip tool
        ezca['LSC-PR_GAIN_GAIN']=0
        log('ready for locking')
            
class PRMI_LOCK_WAIT(GuardState):
    index = 32
    request = False
    @assert_mc_locked
    def main(self):
        ezca['LSC-MICH_TRIG_THRESH_ON']=par.prmicar_mich_trig_upper_threshold
        ezca['LSC-MICH_TRIG_THRESH_OFF']=par.prmicar_mich_trig_lower_threshold
        # Keep DRMI acquisition gains
        self.lock_timer_on = False;  
        srcl_filt().switch('INPUT','OFFSET','OUTPUT', 'OFF', wait=False)

    @assert_mc_locked
    def run(self):
        PSLinputpower_OK()
        if i.PRMI_locked():
            # look for a lock longer than some minimum time before proceeding
            if self.lock_timer_on:
                if self.timer['lockpause']:
                    # After locked, then put gains to final values
                    #mich_filt().ramp_gain(par.prmicar_mich_gain, 2, wait=False)
                    #prcl_filt().ramp_gain(par.prmicar_prcl_gain, 2, wait=False)
                    # BS limiter OFF
                    ezca.switch('SUS-BS_M3_ISCINF_L', 'LIMIT', 'OFF')
                    i.intrix.load()
                    return True
                else:
                    return
            else:
                log('flashed, waiting to proceed')
                self.timer['lockpause'] = 0.5
                self.lock_timer_on = True
        else:
            self.lock_timer_on = False

class OFFLOAD_PRMI(GuardState):
    index = 41
    request = True
    #@nodes.checker()
    @assert_mc_locked
    def main(self):
        if not i.PRMI_locked():
            return 'LOCK_PRMI_CARRIER'
        # Turn on feedback to M1 stages of PRM
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').ramp_gain(par.prm_M1_cross_over, 2, wait=False)
        log('Offloaded PRM.')
        # DRMI turn off CPS FF to BS (so called MICH freeze) 
        ezca['LSC-CPSFF_GAIN'] = 0
        ezca['SUS-BS_M3_ISCINF_L_OFFSET'] = 0
        mich_filt().switch_on('FM2')
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_L').switch_on('INPUT')
        # Ensure the right DRMI gains are set (in case operator changed them for locking)
        log('setting gains for DRMI with arms')
        mich_filt().ramp_gain(par.prmicar_mich_gain, 2, wait=False)
        prcl_filt().ramp_gain(par.prmicar_prcl_gain, 2, wait=False)
        self.timer['wait'] = 2
        self.boost_on = False
        self.SRM_aligned = False
        self.lock_check = False

    #@nodes.checker()
    @assert_mc_locked
    def run(self):
        log('checking PRMI lock')
        #If ISC_DRMI is managed, this code will mean that it will stall if PRMI drops lock.  ISC_LOCK can then issue a new request, either in PRMI_LOCKED or PRMI_2_DRMI transistion. 
        if not i.PRMI_locked():
            log('PRMI unlocked')
            return 'LOCK_PRMI_CARRIER'
        if self.timer['wait']:
            if not self.boost_on:
                log('turning on boost')
                ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').switch_on('FM1')
                self.boost_on = True
            else:
                return True

ENGAGE_DRMI_WFS_CENTERING = gen_ENGAGE_CORNER_WFS_CENTERING('DRMI')
ENGAGE_DRMI_WFS_CENTERING.index = 60

DRMI_WFS_CENTERING = gen_CORNER_WFS_CENTERING('DRMI')
DRMI_WFS_CENTERING.index = 70

class PREP_DRMI_ASC(GuardState):
    index = 78
    @assert_mc_locked
    @assert_drmi_locked
    def main(self):

        ezca['ASC-WFS_SWTCH']=1
        ezca['ASC-WFS_GAIN']=1
        #input matrix
        # MICH AS B 36 Q 
        i.asc_intrix_pit['MICH', 'AS_B_RF36_Q'] = i.asc_intrix_yaw['MICH', 'AS_B_RF36_Q'] = 0
        i.asc_intrix_pit['MICH', 'AS_A_RF36_Q'] = i.asc_intrix_yaw['MICH', 'AS_A_RF36_Q'] = 0
        i.asc_intrix_pit['MICH', 'AS_B_RF45_Q'] = i.asc_intrix_yaw['MICH', 'AS_B_RF45_Q'] = -50
        i.asc_intrix_pit['MICH', 'AS_A_RF36_I'] = 0 # for removing the high power element
        # INP1 REFL9AI - REFLB9I
        i.asc_intrix_pit['INP1', 'REFL_A_RF9_I'] = i.asc_intrix_yaw['INP1', 'REFL_A_RF9_I'] = 1
        i.asc_intrix_pit['INP1', 'REFL_B_RF9_I'] = i.asc_intrix_yaw['INP1', 'REFL_B_RF9_I'] = -1 # 1?
        i.asc_intrix_pit['INP1', 'AS_B_RF45_I']  = i.asc_intrix_yaw['INP1', 'AS_B_RF45_I'] = 0 # remove init align element
        # INP2 REFLB9I
        i.asc_intrix_pit['INP2', 'REFL_B_RF9_I'] = i.asc_intrix_yaw['INP2', 'REFL_B_RF9_I'] = 1
        # PRC1 POPA DC
        i.asc_intrix_pit['PRC1', 'REFL_B_RF9_I'] = i.asc_intrix_yaw['PRC1', 'REFL_B_RF9_I'] = 0 # 1?
        i.asc_intrix_pit['PRC1', 'POP_A_DC'] = i.asc_intrix_yaw['PRC1', 'POP_A_DC'] = 1
        # PRC2 REFLA45I - REFLA9I  #thgis was changed in AUgust 24th to only use refl 9, but I have reverted to the old REFL 45 values 0.83 for both REFL 45s after the power outage oct 21st because we had difficulty engaging without refl45 and it seemed to work with refl 45 
        '''asc_intrix_pit['PRC2', 'REFL_A_RF45_I'] = asc_intrix_yaw['PRC2', 'REFL_A_RF45_I'] = 0.83
        asc_intrix_pit['PRC2', 'REFL_A_RF9_I'] = asc_intrix_yaw['PRC2', 'REFL_A_RF9_I'] = 0.5
        asc_intrix_pit['PRC2', 'REFL_B_RF9_I'] = asc_intrix_yaw['PRC2', 'REFL_B_RF9_I'] = 0.5
        asc_intrix_pit['PRC2', 'REFL_B_RF45_I'] = asc_intrix_yaw['PRC2', 'REFL_B_RF45_I'] = 0.83'''
        #changed back to only refl ( feb 2016)
        i.asc_intrix_pit['PRC2', 'POP_X_I']=i.asc_intrix_yaw['PRC2', 'POP_X_I']=0
        i.asc_intrix_pit['PRC2', 'REFL_A_RF45_I'] = i.asc_intrix_yaw['PRC2', 'REFL_A_RF45_I'] = 0 # get rid of value from input align
        i.asc_intrix_pit['PRC2', 'REFL_A_RF9_I'] = i.asc_intrix_yaw['PRC2', 'REFL_A_RF9_I'] = 1
        i.asc_intrix_pit['PRC2', 'REFL_B_RF9_I'] = i.asc_intrix_yaw['PRC2', 'REFL_B_RF9_I'] = 1
        i.asc_intrix_pit['PRC2', 'REFL_B_RF45_I'] = i.asc_intrix_yaw['PRC2', 'REFL_B_RF45_I'] = 0 # get rid of value from input align
        #AS A 36 I to SRC1 -> SRM
        i.asc_intrix_pit['SRC1', 'REFL_A_RF9_I'] = i.asc_intrix_yaw['SRC1', 'REFL_A_RF9_I'] = 0 # get rid of value from input align
        i.asc_intrix_pit['SRC1', 'AS_A_RF36_I'] =-1
        i.asc_intrix_pit['SRC1', 'AS_B_RF36_I'] = 1 # was 2 (2016/Oct/06) 
        i.asc_intrix_yaw['SRC1', 'AS_B_RF36_I'] = 0.5
        i.asc_intrix_yaw['SRC1', 'AS_A_RF36_I'] = -0.8
        i.asc_intrix_pit['SRC1', 'AS_A_RF45_I']=i.asc_intrix_pit['SRC1', 'AS_B_RF45_I']=0
        i.asc_intrix_yaw['SRC1', 'AS_A_RF45_I']=i.asc_intrix_yaw['SRC1', 'AS_B_RF45_I']=0
        # AS_C to SRC2 -> SR2
        i.asc_intrix_pit['SRC2', 'AS_C_DC'] = i.asc_intrix_yaw['SRC2', 'AS_C_DC'] = 1

        # output matrix
        # INP1 -> IM4
        i.asc_outrix_pit['IM4', 'INP1'] = i.asc_outrix_yaw['IM4', 'INP1'] = 1
        # PRC1 -> PRM
        i.asc_outrix_pit['PRM', 'PRC1'] = i.asc_outrix_yaw['PRM', 'PRC1'] = 1
        i.asc_outrix_pit['PR2', 'PRC1'] = i.asc_outrix_yaw['PR2', 'PRC1'] = 0 # to remove coupling to PR2
        i.asc_outrix_pit['SRM', 'PRC1'] = i.asc_outrix_yaw['SRM', 'PRC1'] = 0 # to remove coupling to SRM
        i.asc_outrix_pit['SR2', 'PRC1'] = i.asc_outrix_yaw['SR2', 'PRC1'] = 0 # to remove coupling to SR2
        i.asc_outrix_pit['IM4', 'PRC1'] = i.asc_outrix_yaw['IM4', 'PRC1'] = 0 # to remove coupling to IM4

        # MICH -> BS
        i.asc_outrix_pit['BS', 'MICH'] = i.asc_outrix_yaw['BS', 'MICH'] = 1
        # SCR1 -> SRM
        i.asc_outrix_pit['SRM', 'SRC2'] = -7.6 # to remove SRM coupling into SR2
        i.asc_outrix_yaw['SRM', 'SRC2'] = 7.1
        i.asc_outrix_pit['SRM', 'SRC1'] = i.asc_outrix_yaw['SRM', 'SRC1'] = 1
        i.asc_outrix_pit['SR3', 'SRC1'] = i.asc_outrix_yaw['SR3', 'SRC1'] = 0
        # SRC2 -> SR2
        i.asc_outrix_pit['SR2', 'SRC2'] = i.asc_outrix_yaw['SR2', 'SRC2'] = 1

        # make sure the suspension offloading is ON (gets missed if first lock is PRMI)
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').switch_on('FM1')
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').switch_on('FM1')
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_L').switch_on('FM1')

        # setup limiters
        ezca['ASC-PRC1_P_LIMIT'] = ezca['ASC-PRC1_Y_LIMIT']=10000
        ezca['ASC-SRC2_P_LIMIT'] = ezca['ASC-SRC2_Y_LIMIT']=1000

        # set up BS feedback to M2 only, hold M1        
        sus_bs_m1_pit_filter().only_on('FM1', 'FM2', 'FM9', 'LIMIT','OUTPUT', 'DECIMATION') #m1 input is turned off
        sus_bs_m1_yaw_filter().only_on('FM1', 'FM9', 'LIMIT','OUTPUT', 'DECIMATION')
        ezca['SUS-BS_M2_LOCK_OUTSW_P'] = 1 
        ezca['SUS-BS_M2_LOCK_OUTSW_Y'] = 1 
        ezca.get_LIGOFilter('ASC-MICH_P').only_on('INPUT', 'FM1','FM3','FM7','FM10','OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-MICH_Y').only_on('INPUT','FM1','FM3','FM4','FM8','FM10','OUTPUT', 'DECIMATION')
        # set up PRM feedback to M1 and M3  
        sus_prm_m1_pit_filter().only_on('FM1','FM9', 'LIMIT','OUTPUT', 'DECIMATION') #m1 input is turned off
        sus_prm_m1_yaw_filter().only_on('FM1','FM9', 'LIMIT','OUTPUT', 'DECIMATION')    
        ezca.get_LIGOFilter('ASC-PRC1_P').only_on('FM2', 'FM3', 'FM4', 'FM8', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-PRC1_Y').only_on('FM2', 'FM3', 'FM4', 'FM7', 'OUTPUT', 'DECIMATION')
        ezca['ASC-PRC1_P_GAIN'] = 1
        ezca['ASC-PRC1_Y_GAIN'] = 1
        # set up PR2 or PR3 feedback to M3 only  
        ezca['SUS-PR3_M3_LOCK_OUTSW_P'] = 1 
        ezca['SUS-PR3_M3_LOCK_OUTSW_Y'] = 1 
        ezca['SUS-PR2_M3_LOCK_OUTSW_P'] = 1 
        ezca['SUS-PR2_M3_LOCK_OUTSW_Y'] = 1 
        # for now match the PR2 gains with PR3
        ezca['SUS-PR3_M3_ISCINF_P_GAIN'] = 2
        ezca['SUS-PR3_M3_ISCINF_P_GAIN'] = 5
        sus_pr3_m1_pit_filter().only_on('FM1','FM2', 'FM9', 'OUTPUT', 'LIMIT','DECIMATION') # PR3 PIT has a 4min decay time to compensate wire heating
        sus_pr3_m1_yaw_filter().only_on('FM1', 'FM9', 'OUTPUT', 'LIMIT','DECIMATION')
        sus_pr2_m1_pit_filter().only_on('FM1', 'FM9', 'OUTPUT', 'LIMIT','DECIMATION')
        sus_pr2_m1_yaw_filter().only_on('FM1', 'FM9', 'OUTPUT', 'LIMIT','DECIMATION')

        # set up IM4 feedback to M3 only  
        filt('SUS-IM4_M1_LOCK_P').only_on('INPUT', 'FM1', 'FM2', 'OUTPUT', 'DECIMATION')
        filt('SUS-IM4_M1_LOCK_Y').only_on('INPUT', 'FM1', 'FM2', 'OUTPUT', 'DECIMATION')     
        ezca['ASC-INP1_P_TRAMP'] = 0
        ezca['ASC-INP1_Y_TRAMP'] = 0
        time.sleep(0.1)
        ezca['ASC-INP1_P_GAIN'] = 0
        ezca['ASC-INP1_Y_GAIN'] = 0
        ezca.get_LIGOFilter('ASC-INP1_P').only_on('INPUT','FM4','FM5','FM10','OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-INP1_Y').only_on('INPUT','FM4','FM5','FM10','OUTPUT','DECIMATION')
        #ezca['ASC-INP1_P_GAIN'] = 1#0.1
        #ezca['ASC-INP1_Y_GAIN'] = 1#-0.01


        # set up SRM feedback to M3 only  
        # set up SRM feedback to M1
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_P').only_on('OUTPUT','LIMIT', 'DECIMATION','FM1','FM8') 
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_Y').only_on('OUTPUT','LIMIT','DECIMATION','FM1','FM8')     
        ezca['SUS-SRM_M3_LOCK_OUTSW_P'] = 1 
        ezca['SUS-SRM_M3_LOCK_OUTSW_Y'] = 1 
        ezca.get_LIGOFilter('ASC-SRC1_P').only_on('FM2', 'FM3', 'FM5','FM10','OUTPUT','DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC1_Y').only_on('FM2', 'FM3', 'FM5', 'FM10','OUTPUT','DECIMATION')
        ezca['ASC-SRC1_P_GAIN'] = -12
        ezca['ASC-SRC1_Y_GAIN'] = -12
        # set up SR2 feedback to M3 only       
        sus_sr2_m1_pit_filter().only_on('OUTPUT','LIMIT', 'DECIMATION','FM2','FM8') 
        sus_sr2_m1_yaw_filter().only_on('OUTPUT', 'LIMIT','DECIMATION','FM2','FM8') 
        ezca['SUS-SR2_M3_LOCK_OUTSW_P'] = 1 
        ezca['SUS-SR2_M3_LOCK_OUTSW_Y'] = 1 
        ezca.get_LIGOFilter('ASC-SRC2_P').only_on('FM1', 'FM2', 'FM3', 'FM4', 'FM9',
                                                  'FM10','OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC2_Y').only_on('FM1', 'FM2', 'FM3', 'FM4', 'FM9',
                                                  'FM10','OUTPUT', 'DECIMATION')
        ezca['ASC-SRC2_P_GAIN'] = 20
        ezca['ASC-SRC2_Y_GAIN'] = 20
 
        # Give back the AS/POP 
        ezca['ASC-AS90_OVER_POP90_GAIN']=1


    @assert_mc_locked
    @assert_drmi_locked
    def run(self):
        return True


##################################################

edges = [
    ('IDLE','DOWN'),
    ('LOCKLOSS', 'DOWN'),
# DRMI edges:
    ('DOWN', 'SET_PRMI'), 
    ('SET_PRMI','LOCK_PRMI_CARRIER'),

#PRMI edges
    ('LOCK_PRMI_CARRIER','PRMI_LOCK_WAIT'),
    ('PRMI_LOCK_WAIT', 'OFFLOAD_PRMI'),

#DRMI ASC edges
    ('ENGAGE_DRMI_WFS_CENTERING','DRMI_WFS_CENTERING'),
    ('DRMI_WFS_CENTERING', 'PREP_DRMI_ASC'),
    ]

##################################################
# SVN $Id:$
# $HeadURL:$
##################################################
